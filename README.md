
Connector de AdamsPay para WooCommerce 
https://adamspay.com

Para instalar el Plugin:

- Copiar carpeta /woocommerce-adamspay a /wp-content/plugins de la instalacion de WordPress
- Activarlo desde los plugins de Wordpress
- Configurarlo desde WooCommerce / Settings / Payments (vas a necesitar haber creado tu aplicación en AdamsPay)

Mas informacion: https://wpavanzado.com/como-instalar-un-plugin-en-wordpress/

Creado por Juan Marcelo Alonso <marceloalonso@gmail.com>
