<?php
namespace AdamsPay;
/**
 *
 * @link              https://adamspay.com/
 * @since             1.0
 * @package           AdamsPay
 *
 * @wordpress-plugin
 * Plugin Name:       AdamsPay Gateway
 * Plugin URI:        https://wiki.adamspay.com/devzone:plugins:wc
 * Description:       AdamsPay payment method for WooCommerce
 * Version:           2.5.7
 * Author:            AdamsPay
 * Author URI:        https://adamspay.com/
 * License:           GPLv3 (https://www.gnu.org/licenses/gpl-3.0.html)
 * Text Domain:       adamspay-wc
 * Domain Path:       /languages
 * WC tested up to: 5.2.2
 * 
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

include_once __DIR__ . '/includes/plugin.php';

APPlugin::allocMainInstance();

function getPlugin():APPlugin {
    return APPlugin::getMainInstance();
}
add_filter( 'plugin_action_links_'.plugin_basename(__FILE__), [APPlugin::class,'wpActionsLink'] );
