=== AdamsPay connector for WooCommerce ===
Contributors: mbalsevich
Tags: ecommerce, payment, woocommerce
Requires at least: 5.4
Tested up to: 5.6.1
Stable tag: 2.5
Requires PHP: 7.0
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

WooCommerce payment gateweway and shipping method that adds AdamsPay services during checkout

== Description ==

This plugin enables you to use the AdamsPay gateway in WooCommerce. You will have to configure your services in https://admin.adamspay.com before being able to use it.

Please refer to our guide in https://wiki.adamspay.com/devzone:plugins:wc for usage.

== Changelog ==

= 2.5.7 =
* Requires phone and number for shipping


= 2.5 =
* Adds spanish translation.
* Adds shipping method
* First version uploaded to Wordpress plugin directory

