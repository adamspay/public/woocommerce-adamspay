<?php
namespace AdamsPay\Template;    // This MUST be the namespace

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

// Place your customized file in the templates\adamspay folder of your theme folder
// {wordpress-root}\wp-content\themes\{yourtheme}\templates\adamspay\{function_name}.php
// and make sure the fqn of the function is AdamsPay\Template\{function_name}
 
// payment_iframe receives 1 param, the URL of the AdamsPay page to display: 

function payment_iframe($htmlSafeUrl,array $options)
{
?><div><iframe style="border:0;padding:0;margin:0;width:100%;height:660px;overflow-y:hidden" scrolling="no" src="<?php echo $htmlSafeUrl; ?>"></iframe></div><?php
    
}

