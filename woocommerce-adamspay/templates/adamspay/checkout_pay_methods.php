<?php
namespace AdamsPay\Template;    // This MUST be the namespace

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

// Place your customized file in the templates\adamspay folder of your theme folder
// {wordpress-root}\wp-content\themes\{yourtheme}\templates\adamspay\{function_name}.php
// and make sure the fqn of the function is AdamsPay\Template\{function_name}


// checkout_pay_methods receives 2 params: 
// the field name and the possible options

function checkout_pay_methods($field_name,array $options)
{
?><div>
<?php
    foreach($options as $optionId=>$optionInfo)
    {
?>
    <div style="padding:6px"><input type="radio" id="ap_pay_service_<?php 
        echo $optionId; 
    ?>" name="<?php 
        echo $field_name; 
    ?>" value="<?php 
        echo  $optionId; 
    ?>"> <label for="ap_pay_service_<?php 
        echo $optionId; 
    ?>"><?php 
        echo esc_html($optionInfo['label']) ;
    ?></label></div>
<?php 
    }
?>
</div><?php
    
}

