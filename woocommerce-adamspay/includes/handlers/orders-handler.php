<?php
namespace AdamsPay;

if ( !defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
    exit; // Exit if accessed directly.
}



include_once ADAMSPAY_INCLUDE_PATH . 'handlers/handler.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/debt.php';
include_once ADAMSPAY_INCLUDE_PATH . 'payment-gateway.php';

use \WP;
use \WC_Order;
use AdamsPay\APHandler;
use AdamsPay\APDebt;

class APOrdersHandler 
extends APHandler {
    
    
    
    function __construct(APPluginModule $module) {
        parent::__construct($module);
    }
    
    function getHandlerSlug():string{
        return APDefs::handlerOrders;
    }
    
            

    function onRequest( WP $wp, string $action, ?string $path ) {
        
        switch( $action ){
            case APDefs::actionPathReturn:
                $this->onReturnFromAdamsPay();
                break;
            
            case APDefs::actionPathNotify:
                $this->onAdamsPayNotification();
                break;
        }
        // If unhandled, silently redirect to HOME
        wp_redirect( home_url());
        exit();
    }
    

    private function onAdamsPayNotification()
    {
        
        $post = json_decode( file_get_contents( 'php://input' ), true );

        $notify = isset( $post['notify'] ) ? $post['notify'] : null;
        
        if( !$notify || !isset($notify['type']) ){
            http_response_code(400);    // Invalid
        }
        else
        if($notify['type'] === 'debtStatus'){
            // Method does not return
            $this->onAdamsPayDebtStatusNotify( isset( $post['debt'] ) ? $post['debt'] : null );
        }
        else {
            http_response_code(202);
        }
        exit();
    }
    
    
    private function onAdamsPayDebtStatusNotify( ?array $apiModel ){
        $gateway = APPaymentGateway::getMainInstance();
        if( !$gateway ){
            http_response_code(500);    // No adams gateway!
        }
        else {
            // Let gateway decide what to do
            // 200 indicates successfull processing
            $gateway->onAdamsPayDebtStatusNotify( $apiModel);
            http_response_code(200);
        }        
        exit();
    }
    
  
    /*
     * User is returning from AdamsPay debt
     */
    private function onReturnFromAdamsPay(){
    

        $objType = APHelper::sanitizedQueryKey('type') ;
        $docId = $objType === 'debt' ? APHelper::sanitizedQueryKey('doc_id') : null;
        $orderId = APDebt::extractOrderIdFromDocId( $docId );
        $order  = $orderId ? wc_get_order($orderId) : null;
        if( empty($order)) {
            do_action( 'logger', __FUNCTION__." order ($orderId) not found");
            return;
        }
        
        $gateway = APPaymentGateway::getMainInstance();
        if( !$gateway ){
            return;
        }
        // Let gateway decide where to go
        $gateway->onReturnFromAdamsPay($order);
        
    }

    


    
        
   
    
   

}

