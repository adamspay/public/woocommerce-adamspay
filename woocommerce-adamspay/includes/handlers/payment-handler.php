<?php
namespace AdamsPay;

if ( !defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
    exit; // Exit if accessed directly.
}



include_once ADAMSPAY_INCLUDE_PATH . 'handlers/handler.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/debt.php';
include_once ADAMSPAY_INCLUDE_PATH . 'util/helper.php';
include_once ADAMSPAY_INCLUDE_PATH . 'payment-gateway.php';

use AdamsPay\APHandler;
use AdamsPay\APDebt;
use AdamsPay\APHelper;

class APPaymentHandler 
extends APHandler {
    
    
    
    function __construct(APPluginModule $module) {
        parent::__construct($module);
    }
    
    function getHandlerSlug():string{
        return APDefs::handlerPayment;
    }
    
            
     //
    // Handle a short code 
    // Available short coudes should be returned by APHandler::getShortCodeMap()
    function onShortCodeRender(?array $attr, ?string $content, string $shortCode):string {
        
        switch( $shortCode )
        {
        case APDefs::shortCodePayment:
            return $this->onRenderPayment($attr,$content);
        }
        return parent::onShortCodeRender($attr, $content, $shortCode);
    }
   
    private function onRenderPayment(?array $attr,?string $content):string {
        
        $orderId = APHelper::sanitizedRequestKey('order');
        if( !$orderId ){
            return '<div style="padding:20px;margin:20px 0;border:1px solid #eee;">Adams Payment frame here</div>';
        }
        
        $order = wc_get_order($orderId);
        if( !$order ){
            return '<div style="padding:20px;margin:20px 0;border:1px solid #eee;">Order '.$orderId.' not found</div>';
        }
        $debt = APDebt::readFromOrder($order);
        if( !$debt ){
            return  "Order($orderId): Debt not found";
        }
        $extraParams = ['display'=>'embed'];
        $ps = APHelper::sanitizedRequestKey('pay_service');
        if( $ps !== null ){
            $extraParams['pay_service'] = $ps;
        }
        
        // Get DEBT url (do NOT request embed URL)
        $url = $this->getPlugin()->getPaymentGateway()->buildDebtPayUrl($debt,['may_embed_as_order'=>null,'may_identify_user'=>true,'mute_adams_event'=>true,'extra_params'=>$extraParams]);
        
        $iframe = APPlugin::includeTemplate('payment_iframe');
        if( !$iframe ){
            // no template: Print the URL
            $ix= strpos($url,'?');
            return '<a href="'.$url.'">'.($ix===false?$url:substr($url,0,$ix)).'</a>';
            
        }
        ob_start();
        
        call_user_func($iframe,$url,[]);
?><script>
 
(function($){
    
    var verbose=<?php echo APDefs::isDevMachine() && false ? 'true':'false'; ?>
        , loadTimeout = window.setTimeout(onLoadTimeout,10000)
        , isPreview = <?php echo APHelper::sanitizedQueryKey( 'preview') === 'true'? 'true':'false'; ?>
        , sessionEnded= false;
    
    window.addEventListener("message", onMessageEvent, false);
    
    function onMessageEvent(event) {
        var adamsEvent = event.data ? event.data.adamsEvent : null;
        if( !adamsEvent )return ;   // Not an adamsEvent
        
        if( verbose ){
            console.log("onMessageEvent(ended:"+sessionEnded+")="+JSON.stringify(adamsEvent));
        }
        if( sessionEnded ){
            return;
        }
        if( adamsEvent.event === "pageReady" ){
            clearLoadTimeout();
            if( adamsEvent.pageId.indexOf("error") !== -1 ){
                endPayment(adamsEvent,{"success":false,"reason":"error"}); // An error page was loaded!
            } 
        }
        else
        if( adamsEvent.event === "sessionEnd" ){
            sessionEnded = true;
            var payScene = adamsEvent.intentInfo ? adamsEvent.intentInfo.payScene : null;
            var paid = payScene === "paid";
            endPayment(adamsEvent,{"success":paid,"reason":"cancelled"}); // An error page was loaded!
        }   
    }
    
    function onLoadTimeout(){
        loadTimeout = 0;
        endPayment(null,{"success":false,"reason":"timeout"}); // An error page was loaded!
    }
    
    function endPayment( adamsEvent, params ){
        if( verbose )console.log("endPayment.params="+JSON.stringify(params));
        if( !isPreview ){
            var endUrl = adamsEvent && adamsEvent.closePageLink ? adamsEvent.closePageLink.url : null;
            if( verbose )console.log("endPayment.endURL=" + endUrl);
            if( endUrl ){
                window.open(endUrl,"_top");
            }
            else {
                if( verbose )console.log("NO_URL");
            }
        }
    }
    function clearLoadTimeout(){
        if( loadTimeout ){
            window.clearTimeout(loadTimeout);
            loadTimeout = 0;
        }
    }
})(jQuery);

</script><?php
        return ob_get_clean();
    }
}

