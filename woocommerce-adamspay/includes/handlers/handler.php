<?php
namespace AdamsPay;

/**
 * Base class for handling request and filter handlers
 */

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
	exit; // Exit if accessed directly.
}

// Defs generates include path
include_once ADAMSPAY_INCLUDE_PATH . 'util/helper.php';
include_once ADAMSPAY_INCLUDE_PATH . 'util/plugin-module.php';

use AdamsPay\APDefs;
use AdamsPay\APPluginModule;
use AdamsPay\APHelper;

use WP;

abstract class APHandler 
implements APPluginModule {

    const fieldFormHandlerSlug = 'adamspayform'
        , fieldFormShortCode = 'adamspayshortcode'
        , fieldFormNonce = 'adamspaynonce'
        , shortCodePayView = 'adamspay_pay_view';
    
    
    
    private static 
        $_handlers = [];
    private 
      $plugin
    , $requestCache = []
    , $postResult = null;
 
    
    // ----------------------------------------------------------
    // Handler implementation
    //
    abstract function getHandlerSlug():string;
    
    
    // Handle an action. 
    // Action is the first portion of the URL for this plugin
    // /{plugin-slug}/{action-slug}/{path}
    // Available action slugs should be returned by APHandler::getRequestActionMap()
    function onRequest( WP $wp, string $actionSlug, ?string $path ) {
        wp_die('Error: '.\get_class($this).'\\'.__FUNCTION__.'('.$actionSlug.') not implemented');
    }
    
    //
    // Handle a short code 
    // Available short coudes should be returned by APHandler::getShortCodeMap()
    function onShortCodePost( WP $wp ) {}    // Post should not return an error
    function onShortCodeRender(?array $attr, ?string $content, string $shortCode):string {
        return 'Error: '.\get_class($this).'\\'.__FUNCTION__.'('.$shortCode.') not implemented';
    }
    
    
    function getHandlerPath():string {
        return $this->getPlugin()->getHandlerPath( $this->getHandlerSlug() );
    }
    
    
    /**
     * return array of handlers that should be created during plugin initialization
     * @return string[]
     */
    static function getAutoBootHandlers():array {
        return [];
    }


    /**
     * array is keyed[shortCode] => handlerSlug
     * @return array
     */
    static function getShortCodeMap():array {
        return [APDefs::shortCodePayment=>APDefs::handlerPayment];
    }

    
    /**
     * array is keyed[actionSlug] => handlerSlug
     * when a request is received with /{pluginPath}/{actionPath}
     * the handler will be created and onRequest() invoked
     * @return array
     */
    static function getRequestActionMap():array {
        return [
             APDefs::actionPathNotify=>APDefs::handlerOrders
            ,APDefs::actionPathReturn=>APDefs::handlerOrders
        ];
    }
    
    
    // Instance of the plugin
    function getPlugin():APPlugin {
        return $this->plugin;
    }
    

    function getHandlerWithSlug( string $slug ):?self {
        return self::allocHandlerWithSlug($this, $slug);
    }


    static function allocHandlerWithSlug(APPluginModule $module,  string $slug):?self{

        if(isset(self::$_handlers[$slug]))
            return self::$_handlers[$slug];
        
        switch( $slug ){
        case APDefs::handlerOrders:{
include_once ADAMSPAY_INCLUDE_PATH . 'handlers/orders-handler.php' ;
                $handler = new APOrdersHandler( $module);
            }
            break;
        
       
        case APDefs::handlerPayment:{
include_once ADAMSPAY_INCLUDE_PATH . 'handlers/payment-handler.php' ;
                $handler = new APPaymentHandler( $module);
            }
            break;
        default:
            return null;
        }
        self::$_handlers[$slug] = $handler;
        return $handler;
    }
    
    
    
    
    protected function getPostResultFor( string $shortCode ):?array {
        return isset( $this->postResult[$shortCode]) ? $this->postResult[$shortCode] : null;
    }
    protected function setPostResultFor( ?string $shortCode, ?array $result ) {
        $this->postResult = empty($shortCode) || empty($result) ? null : [$shortCode=>$result];
    }

    
    protected function isMyPost($shortCodeList):?string {
        

        if( APHelper::sanitizedPostKey(self::fieldFormHandlerSlug) === $this->getHandlerSlug() )
        {
            $postShortCode = APHelper::sanitizedPostKey(self::fieldFormShortCode);
            $postNonce = APHelper::sanitizedPostKey(self::fieldFormNonce);
            if( $postShortCode && $postNonce ){
                if( is_string($shortCodeList) )$shortCodeList =explode(',',$shortCodeList);
                foreach( $shortCodeList as $tryShortCode )
                {
                    if( $postShortCode === $tryShortCode && wp_verify_nonce(  $postNonce , $this->getHandlerSlug().$tryShortCode ) ){
                        return $postShortCode;
                    }
                }
            }
        }
        return null;
        
    }
    protected function getWidgetFormFields(string $shortCode):string {
        return '<input type="hidden" name="' . self::fieldFormHandlerSlug
                . '" value="' . $this->getHandlerSlug()
                . '"><input type="hidden" name="' . self::fieldFormShortCode
                .'" value="'.$shortCode
                .'">' . wp_nonce_field($this->getHandlerSlug().$shortCode, self::fieldFormNonce, true, false);

    }
    

  
    

    /**
     * 
     * @param string $cacheKey
     * @throws APException
     */
    function checkResultCache(string $cacheKey) {
        if( isset($this->requestCache[$cacheKey]) ) {
            $data =  $this->requestCache[$cacheKey];
        }
        else {
            $data = get_transient($cacheKey.'_adamspay_wc');
        }
        
        return $data ? self::fromCacheData( $data ) : null;
    }
    
    function setResultCache(string $cacheKey, $data, int $minutes) {
        $cacheData = self::toCacheData($data);
        $this->requestCache[$cacheKey] = $cacheData;
        if( $minutes > 0 ){
            set_transient($cacheKey.'_adamspay_wc', $cacheData , $minutes * 60 );
        }
    }

    private static function toCacheData( $data ):array {
        if(is_object($data)){
            if( is_a($data,APError::class))
                return ['error'=>$data];

            if( is_a($data,APException::class))
                return ['error'=>$data->getError()];
            
            if( is_a($data,\Exception::class))
                return ['error'=> APError::toError($data)];
        }
        return ['value'=>$data];
    }
    private static function fromCacheData( array $data ){
        if( isset($data['error'])){
            throw APException::fromError($data['error']); // AP handlers always return errors and APException
        }
        return $data['value'];
    }
    
    protected function __construct(APPluginModule $module) {
         $this->plugin = $module->getPlugin();
    }
    

 }

