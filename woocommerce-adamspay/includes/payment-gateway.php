<?php
namespace AdamsPay;

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
    exit;
}

include_once ADAMSPAY_INCLUDE_PATH . 'plugin.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/pay-config.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/debt.php';
include_once ADAMSPAY_INCLUDE_PATH . 'util/helper.php';
include_once ADAMSPAY_INCLUDE_PATH . 'util/client.php';
include_once ADAMSPAY_INCLUDE_PATH . 'util/plugin-module.php';
include_once ADAMSPAY_INCLUDE_PATH . 'util/main-instance-module.php';

use AdamsPay\APDefs;
use AdamsPay\APPlugin;
use AdamsPay\APClient;
use AdamsPay\APPayConfig;
use AdamsPay\APDebt;
use AdamsPay\APHelper;

use \WC_Order;


// Note that AdamsPay notifications sent to payment gateway are handled
// by APOrderHandler

/**
 * @property APPayConfig $apPayConfig
 */

class APPaymentGateway
extends \WC_Payment_Gateway 
implements APPluginModule {


    use APMainInstanceModule;  // Is connected to main instance
    
    const gatewayId='wc_adamspay';
    private $apPayConfig, $preSelectedService, $modeSuffix;

    static function getMainInstance():?APPaymentGateway {
        if(is_callable('WC')){
            $gateways =  WC()->payment_gateways->payment_gateways();
            return isset( $gateways[ APPaymentGateway::gatewayId]) ? $gateways[ APPaymentGateway::gatewayId] : null;
        }
        return null;
    }
    
    
    static function onPluginInit( APPlugin $plugin ){
        add_filter('woocommerce_payment_gateways', [self::class,'wcPaymentMethods']);
    }
    
    function getPayConfig():APPayConfig {
        $this->debugLog(__FUNCTION__.'.havePayConfig:'.($this->apPayConfig?'YES':'#CREATE'));
        if( !$this->apPayConfig ){
            if( empty( $this->settings ) ){     
                $this->init_settings(); 
            }
            $this->apPayConfig = new APPayConfig( $this->settings );
        }
        return $this->apPayConfig;
    }

    // AdamsPay extensions
    function getAdamsPayClient():APClient {
       return $this->getPlugin()->getAdamsPayClient( $this->getPayConfig() );
    }
    
    

    function __construct() {
        
        // WC_Settings_API
        // 
        $this->id = self::gatewayId;
        $this->has_fields = false;
        $this->modeSuffix =  APDefs::isDevMachine() ?' (dev)':'(std)';
        
        $this->init_form_fields();  // form_fields
        
        // WC_Payment_Gateway
        $this->init_settings();     // settings
        

        // Items depend on settings
        
        
        $this->method_title = __( 'AdamsPay', 'wc-adamspay' ).$this->modeSuffix;
        $this->method_description = __( 'AdamsPay multi provider payment gateway', 'wc-adamspay' );
        // Define user set variables.
        $this->title        = ($this->_settingsStr( 'title' ) ?? 'AdamsPay');
        $this->description  = $this->_settingsStr( 'description' );
        
        
        add_action( 'woocommerce_admin_order_data_after_order_details', [$this,'wcAddInfo2AdminOrderDetails'] );
//        add_action( 'woocommerce_order_details_after_order_table_items', [$this,'wcAddInfo2UserOrderDetails'] );	// Replaced by wcAddInfoToOrderItems
        add_filter( 'woocommerce_get_order_item_totals', [$this,'wcAddInfoToOrderItems'], 10, 3 );

        add_action( 'woocommerce_order_status_cancelled', [$this, 'wcOrderCancelled']  );

        add_filter( 'woocommerce_my_account_my_orders_actions', [$this,'wcUserOrderActions'] , 10, 2 );
        add_filter( 'woocommerce_thankyou_order_received_text', [$this,'wcOrderReceivedText'], 20, 2 );
        
        // Must deprecate this filter after checking if Cerro does not use it
        add_filter( 'adamspay_build_debt_pay_url', [ $this, 'buildDebtPayUrl' ], 10, 2 );

        // SAving of this form
        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, [ $this, 'wcUpdateOptions' ] );


    }
    
    // --------------------------------------------------------
    // WC_Settings_API
    
    function init_form_fields() {
        parent::init_form_fields();
        $this->form_fields = $this->allocFormFields();
    }
    
    
    
    function wcUpdateOptions(){
        $this->debugLog(__FUNCTION__.'.havePayConfig[begin]:'.($this->apPayConfig?'YES':'no'));
        $this->settings=[];
        $this->process_admin_options();
        $this->debugLog(__FUNCTION__.'.havePayConfig[after_process]:'.($this->apPayConfig?'YES':'no'));
        $this->invalidateConfig();   // Force config rebuild cache
        $this->debugLog(__FUNCTION__.'.havePayConfig[after_invalidate]:'.($this->apPayConfig?'YES':'no'));
        $this->apAfterSettingsUpdated();
        $this->debugLog(__FUNCTION__.'.havePayConfig[end]:'.($this->apPayConfig?'YES':'no'));
    }
    
    
    
    // --------------------------------------------------------
    // WC_Payment_Gateway
    

    // WC_Payment_Gateway
    static function wcPaymentMethods($methods) {
        $methods[] = self::class;
        return $methods;
        
    }
    
    // WC_Payment_Gateway
    function init_settings() {
        parent::init_settings();
        $this->apAfterSettingsLoaded();
    }
    
    // WC_Payment_Gateway
    function payment_fields() {

        
        $payConfig = $this->getPayConfig();
        if( $payConfig->getPreSelectPayMethod() === APPayConfig::payMethodSelectCheckoutField && null !== ($serviceMap = $payConfig->getAppServiceMap()) ){
            
            $payOptions = [];
            $allowedMethods = [
                     'wire'=>[]
                    ,'sim'=>[]
                    ,'bancardqr'=>['label'=>'Pago con QR']
                    ,'bancardvpos1'=>['label'=>'Tarjeta de crédito']
                    ,'bancardzimple'=>[]
                    ,'tigomoney'=>[]
                    ,'bpersonal'=>[]
                    ];
            
            foreach( $serviceMap as $serviceSlug=>$serviceInfo )
            {
                if( !empty($serviceInfo['isPay']) 
                    && array_key_exists($serviceSlug, $allowedMethods)
                    && !empty($serviceInfo['state']) 
                    && array_search($serviceInfo['state'] ,['off','disabled']) === false 
                ){
                    $allow = $allowedMethods[$serviceSlug];
                    if( !empty($allow['label']) ){
                        $label = $allow['label'];
                    }
                    else 
                    if( !empty($serviceInfo['optLabel']) ){
                        $label = $serviceInfo['optLabel'];
                    }
                    else
                    if( !empty($serviceInfo['label']) ){

                        $label =  $serviceInfo['label'];
                    }
                    else {
                        $label = $serviceSlug;
                    }
                    $payOptions[$serviceSlug] = ['label'=>$label];
                }
            }
//            if( $payConfig->getActiveEnv() !== 'prod'){
//                $payOptions['sim'] = ['label'=>'Simulador'];
//            }
            
            if( count($payOptions) && ($template = APPlugin::includeTemplate('checkout_pay_methods')) !== null)
            {
                ?><input type="hidden" name="ap_pay_service_present" value="1"><?php
                call_user_func($template,'ap_pay_service',$payOptions);
                return;
            }
            
        }
        return parent::payment_fields();
    }
    
    
    
    function validate_fields() {
        if( $this->getPayConfig()->getPreSelectPayMethod() === APPayConfig::payMethodSelectCheckoutField ){
            $methodPresent = APHelper::sanitizedPostKey('ap_pay_service_present') === '1';
            $this->preSelectedService = $methodPresent ? APHelper::sanitizedPostKey('ap_pay_service'): null;
        }
        return true;
    }


    // WC_Payment_Gateway
    function admin_options(){
        $this->debugLog(__FUNCTION__.'.havePayConfig:'.($this->apPayConfig?'YES':'no'));
        $this->apBeforeRenderAdminForm();
        parent::admin_options();
    }
   
    // WC_Payment_Gateway
    function get_transaction_url( $order ) 
    {
        if( null !== ($debt = APDebt::readFromOrder($order)) && null !== ($url=$debt->getPayUrl())){
            $this->view_transaction_url = $url;
        }
        return parent::get_transaction_url( $order );
    }
    
    // WC_Payment_Gateway
    function process_payment( $orderId ) {
        $order = new WC_Order( $orderId );
        $apiModel = $this->createDebtForOrder( $order);
        if( $apiModel ){
            $pageUrl = null;
            $payConfig = $this->getPayConfig();
            $opts = ['may_identify_user'=>true,'mute_adams_event'=>false];
            if( $this->preSelectedService ){
                $opts['pay_service']=$this->preSelectedService;
            }
            $debtUrl = $this->buildDebtPayUrl( APDebt::readFromOrder( $order ), $opts );
            $pageId=$payConfig->getEmbedPaymentPage();
            if( !empty($pageId) && ($pageUrl = \get_permalink($pageId)) ){
                $cat = strpos($pageUrl,'?') !== false ? '&' : '?';
                $pageUrl .= $cat . 'order='.$orderId.'&pay_service='.$this->preSelectedService;
            }
            return [
                'result'   => 'success',
                'redirect' => $pageUrl ?: $debtUrl 
            ];
            
        }else{
            $htmlErr = esc_html($this->lastErr);
            wc_add_notice("Error: $htmlErr","error");
            return [];
        }

    }
    
    
    // WC_Settings_API
    // Calls this this field type "ap_page_select"
    function generate_ap_page_select_html( $key, $data){
        
        
        $field_key = $this->get_field_key( $key );
        $args = array(
            'name'             => $field_key,
            'id'               => $field_key,
            'sort_column'      => 'menu_order',
            'sort_order'       => 'ASC',
            'show_option_none' => isset($data['_none']) ? $data['_none'] : __('None', 'wc-adamspay' ),
//            'class'            => $value['class'],
            'echo'             => true,
            'selected'         => absint( @$this->settings[$key] ),
             'post_status'      => 'publish,private,draft',
        );
        
        ob_start();
?><tr valign="top"><th scope="row" class="titledesc"><label><?php echo $data['title']; ?></label></th>
<td class="forminp"><?php

        wp_dropdown_pages( $args );
        ?><div style="margin-top:6px">Page <b>MUST</b> contain the short code <code>[<?php echo APDefs::shortCodePayment; ?>]</code></div></td>
</tr><?php
        return ob_get_clean();
    }
    
    // WC_Settings_API
    // Calls this this field type "ap_urls"
    function generate_ap_urls_html( $key, $data){
            ob_start();
            
/* </table><table class="form-table"> */
            
            foreach($data['adamspay']['urls'] as $entry)
            {
?><tr valign="top"><th scope="row" class="titledesc"><label><?php echo $entry['label']; ?> <?php 
                if( isset($entry['desc_tip'])){
                    echo $this->get_tooltip_html( $entry );
                }
   ?></label></th>
<td class="forminp"><fieldset><?php
                if( isset($entry['asLink']) ){
                    echo '<a target="_blank" href="',$entry['value'],'">',esc_html($entry['value']),'</a>';
                }
                else
                {
                    echo '<input type="text" READONLY value="',esc_html($entry['value']),'">'; 
                }
                if( isset($entry['description'])){
                    echo $this->get_description_html( $entry ); 
                }
?></fieldset></td</tr><?php
            }
            return ob_get_clean();
    }
    

    // ------------------------------------------------------------------------
    // Fields for settings form

    private function allocFormFields() {

        $envList = array(
                             'test'	=> __('Test', 'wc-adamspay' ),
                             'prod'	=> __('Production', 'wc-adamspay' )
                     );
        if( APDefs::isDevMachine() ){
            
               $envList['dev'] = __('Development', 'wc-adamspay' );
               $envList['local'] = __('Local', 'wc-adamspay' );
            
        }

        return array(
             'enabled' => array(
                     'title'	=> __( 'Enable / Disable', 'wc-adamspay' ),
                     'label'	=> __( 'Enable this payment gateway', 'wc-adamspay' ),
                     'type'	=> 'checkbox',
                     'default'=> 'no',
             ),
             'title' => array(
                     'title'	=> __( 'Title', 'wc-adamspay' ),
                     'type'      => 'text',
                     'desc_tip'	=> __( 'Payment method title displayed in checkout process.', 'wc-adamspay' ),
                     'default'	=> __( 'AdamsPay', 'wc-adamspay' ),
             ),
             'description' => array(
                     'type'	=> 'textarea',
                     'title'	=> __( 'Description', 'wc-adamspay' ),
                     'desc_tip'	=> __( 'Payment method information displayed in checkout process.', 'wc-adamspay' ),
                     'default'	=> __( 'Adams lets you select your payment method', 'wc-adamspay' ),
                     'css'      => 'max-width:450px;'
             ),
             APPayConfig::optPreSelectPayMethod => array(
                     'title'    => __('Preselect pay method', 'wc-adamspay' ),
                     'type'     => 'select',
                     'desc_tip'	=> __( 'Will allow preselection of payment method during checkout', 'wc-adamspay' ),
                     'default'  => '',
                     'options'  => array(
                        ''	=> __('No', 'wc-adamspay' ),
                         APPayConfig::payMethodSelectCheckoutField=>__('yes, as checkout field', 'wc-adamspay' )
                     )
             ),
             APPayConfig::optEmbedPaymentPage=> array(
                     'title'	=> __( 'Embedded payment page', 'wc-adamspay' ),
                     'type'	=> 'ap_page_select',
                     '_none'   => 'None (Will redirect to AdamsPay)'
                 )
            ,
             'ap_api_title' => array(
                     'title'	=> __( 'Merchant application', 'wc-adamspay' ),
                     'type'	=> 'title'
             ),
             APPayConfig::optActiveEnv => array(
                     'title'         => __( 'Environment', 'wc-adamspay' ),
                     'label'         => __( 'Env', 'wc-adamspay' ),
                     'type'			=> 'select',
                     'default'       => 'test',
                     'options'		=> $envList,
                     'desc_tip'	=> __( 'Selects the AdamsPay environment to use', 'wc-adamspay' ),
             ),
             APPayConfig::optApiKey => array(
                     'title'	=> __( 'Api Key', 'wc-adamspay' ),
                     'type'	=> 'text',
                     'desc_tip'	=> __( 'API Key of your AdamsPay application. Copy it from the AdamsPay site', 'wc-adamspay' ),
             ),
             APPayConfig::optApiSecret => array(
                     'title'	=> __( 'Api Secret', 'wc-adamspay' ),
                     'type'	=> 'password',
                     'desc_tip'	=> __( 'API Secret of your AdamsPay application. Copy it from the AdamsPay site', 'wc-adamspay' ),
             ),
             'ap_urls' => array(
                 'title'	=> '',
                 'type'	=> 'ap_urls',
                 'adamspay'  => null // Loaded by beforeRenderAdminForm
             ),
             'ap_debt_title' => array(
                     'title'	=> __( 'Debts', 'wc-adamspay' ),
                     'type'	=> 'title',
                      'description'=> __( 'Controls how the plugin generates debts for your orders', 'wc-adamspay' )
            ),
             APPayConfig::optDebtLabel => array(
                     'title'	=> __( 'Debt label', 'wc-adamspay' ),
                     'type'	=> 'text',
                     'desc_tip'=> __( 'Title for the debt in the AdamsPay site', 'wc-adamspay' ),
                     'default'=> get_bloginfo("name")
             ),
             APPayConfig::optDebtIdPrefix => array(
                     'title'	=> __( 'ID prefix', 'wc-adamspay' ),
                     'type'	=> 'text',
                     'desc_tip'=> __( 'The IDs for debts generated by this site will have this prefix', 'wc-adamspay' ),
                     'default'=> null
             ),
             APPayConfig::optDebtValidity => array(
                     'title'         => __('Validity time', 'wc-adamspay' ),
                     'type'          => 'select',
                     'desc_tip'	=> __( 'Time that a debt link will remain valid after user initiates payment', 'wc-adamspay' ),
                     'default'       => 'P1D',
                     'options'		=> array(
                             'PT10M'	=> __('Ten (10) Minutes', 'wc-adamspay' ),
                             'PT1H'	=> __('One (1) Hour', 'wc-adamspay' ),
                             'P1D'	=> __('One (1) Day', 'wc-adamspay' ),
                             'P10D'	=> __('Ten (10) Days', 'wc-adamspay' ),
                             'P1M'	=> __('One (1) Month', 'wc-adamspay' ),
                             'P1Y'	=> __('One (1) Year', 'wc-adamspay' )
                     )
             ),
             'ap_unpaid' => array(
                     'title'	=> __( 'Unpaid debts', 'wc-adamspay' ),
                     'type'	=> 'title',
                     'description'=>__('What to do if user returns without completing payment', 'wc-adamspay' ),
             ),
             APPayConfig::optUnpaidOrderStatus => array(
                     'title'         => __('Set order status', 'wc-adamspay' ),
                     'type'          => 'select',
                     'desc_tip'	=> __( 'Sets the ORDER status when the user returns from payment WITHOUT paying', 'wc-adamspay' ),
                     'default'       => '0',
                     'options'		=> array(
                        '0'	=> __('No (Do not modify the order status)', 'wc-adamspay' ),
                        'on-hold' => __('Set the order to "On hold"', 'wc-adamspay' ),
                        'cancelled' => __('Cancel the order', 'wc-adamspay' ),
                     )
             ),		
             APPayConfig::optUnpaidReturnTo => array(
                     'title'         => __('Return page', 'wc-adamspay' ),
                     'type'          => 'select',
                     'desc_tip'	=> __( 'Which page will be presented', 'wc-adamspay' ),
                     'default'       => '0',
                     'options'		=> array(
                        '0'	=> __('Default thank you (order-received)', 'wc-adamspay' ),
                        '@checkout-payment' => __('Checkout payment (order-pay)', 'wc-adamspay' ),
                        '@account-view-order' => __('User\'s account "view order" (view-order)', 'wc-adamspay' ),
                     )
             ),		
            
            
             'ap_opts_title' => array(
                     'title'	=> __( 'Options', 'wc-adamspay' ),
                     'type'	=> 'title'
             ),
             APPayConfig::optUserLink => array(
                     'title'         => __('User transfer', 'wc-adamspay' ),
                     'type'          => 'select',
                     'desc_tip'	=> __( 'Logged-in user info will be passed to AdamsPay during payment', 'wc-adamspay' ),
                     'default'       => 'no',
                     'options'		=> array(
                        'no'	=> __('Disabled', 'wc-adamspay' ),
                        APPayConfig::userLinkInfo 	=> __('Yes, but information will not be stored', 'wc-adamspay' ),
                        APPayConfig::userLinkFull 	=> __('AdamsPay will remember the user (required for cards)', 'wc-adamspay' )
                     )
             ),		
             'ap_msg_title' => array(
                     'title'	=> __( 'Messages', 'wc-adamspay' ),
                     'type'	=> 'title',
                     'description'=>__('Override order messages or leave blank for default.','wc-adamspay')
             ),
             APPayConfig::optMsgPrefix.APPayConfig::msgOrderRecvPaid => array(
                     'title'	=> __( 'Paid order msg', 'wc-adamspay' ),
                     'type'	=> 'text',
                     'desc_tip'=> __( 'Overrides "order received" message for PAID orders', 'wc-adamspay' ),
                     'default'=> __('Thank you, your <a href="{{payurl}}">payment</a> has been received','wc-adamspay')
             ),
             APPayConfig::optMsgPrefix.APPayConfig::msgOrderRecvPending => array(
                     'title'	=> __( 'Pending order msg', 'wc-adamspay' ),
                     'type'	=> 'text',
                     'desc_tip'=> __( 'Overrides "order received" message for PENDING orders. Use {{payurl}} to add the debt URL.', 'wc-adamspay' ),
                     'default'=> 'Order received but <a href="{{payurl}}">payment</a> is pending'
             ),
             APPayConfig::optMsgPrefix.APPayConfig::msgOrderDetailDebt => array(
                     'title'	=> __( 'Order detail link', 'wc-adamspay' ),
                     'type'	=> 'text',
                     'desc_tip'=> __( 'Adds link to paid debt in the order details. Leave blank to remove link', 'wc-adamspay' ),
                     'default'=> 'Payment info:'
             ),
        );

    }
    
   
    function validate_text_field( $key, $value ) {
        $value = parent::validate_text_field($key, $value);
        $len = strlen($value);
        switch($key)
        {
        case APPayConfig::optDebtIdPrefix:
            $slug = preg_replace( '/[^a-z0-9]/', '', strtolower( $value ) );
            if( $slug > 10 )return substr($slug,0,10);
            return $slug;
            
        case APPayConfig::optDebtLabel:
            if( $len > 32 )return substr($value,0,32);
            break;
            
        }
            
        return $value;
    }
    function get_amount($customer_order){
            return $customer_order->order_total;
    }
    
    
    private $lastErr;
    
    private function getLastError() : ?string {
        return $this->lastErr;
    }
    
    
    
    private function createDebtForOrder( \WC_Order $order ):?array {
        $this->lastErr = null;
        $orderId = $order->get_id();
        $config = $this->getPayConfig();
        
        $debt = APDebt::readFromOrder( $order );
        if( !$debt ){
            $docId = APDebt::composeDocId( $config->getDebtIdPrefix() , $orderId) ;
            $debt = APDebt::allocNew( $docId, $order );
            
        }
        else {
            $docId = $debt->getDocId();
        }
        
        $client = $this->getAdamsPayClient();
        if( $order->is_paid() ){
            
            // Already paid, just retrieve URL
            try {
                $apiModel = $client->getDebt( $docId );
            }
            catch(APException $e ){
                $apiModel = null;
                $err = $e->getError();
                $this->lastErr = $e->getMessage().' ['.$err->getInfoCode().']';
            }
            
        }
        else {
            $utzTimeZone = new \DateTimeZone('UTC');


            $reqLabel = $config->getDebtLabel();
            if( empty($reqLabel) ) $reqLabel = get_bloginfo("name");

            $reqValidStart = new \DateTimeImmutable('now',$utzTimeZone);
            $reqValidEnd = $reqValidStart->add(new \DateInterval(strtoupper($config->getDebtValidity())));

            $reqModel = [
                'docId'=>$docId
                ,'label'=>$reqLabel
                ,'amount'=>[
                         'currency'=>$order->get_currency()
                        ,'value'=>\number_format($order->get_total(),4,'.','')
                    ]
                ,'objStatus'=>[
                    'status'=>'active'
                ]
                ,'validPeriod'=>[
                    'start'=>$reqValidStart->format(\DateTime::ATOM)
                    ,'end'=>$reqValidEnd->format(\DateTime::ATOM)
                ]

            ];

            try {
                $apiModel = $client->postDebt( $reqModel , true );
                $order->add_order_note( sprintf(__( 'AdamsPay debt %s created', 'wc-adamspay' ),$docId) );
            }
            catch(APException $e ){
                $apiModel = null;
                $err = $e->getError();
                $this->lastErr = $e->getMessage().' ['.$err->getInfoCode().']';
            }
            // Could not create/update the DEBT, maybe because it has already been paid
            if( !$apiModel ){
                try {
                    $apiModel =$client->getDebt($docId);
                    
                    // Discard create error
                    $this->lastErr = null;
                }
                catch(APException $e ){
                    // Could not retrieve debt: Leave create error as lastErr
                }
            }
        }
        
        if( $apiModel ){
            $debt->updateFromApiModel( $apiModel );
            $debt->saveToOrder( $order );
            return $apiModel;
            
        }
        return null;
    }

    
  
    function buildDebtPayUrl(  APDebt $debt, ?array $opts=[] ) {
        $payClient = $this->getAdamsPayClient();
        $payConfig = $payClient->getPayConfig();
        $payParams = APHelper::forceArray(@$opts['extra_params']);
        if( !empty($opts['pay_service'])){
            $payParams['pay_service'] = $opts['pay_service'];
        }
        if(!empty($opts['may_embed_as_order']) 
            && is_a($opts['may_embed_as_order'], \WC_Order::class) 
            && ($embedPageId= $payConfig->getEmbedPaymentPage()) 
            && ($embedPageUrl = \get_permalink($embedPageId)) ){
                /** @var WC_Order $order */
            $order = $opts['may_embed_as_order'];
            $cat = strpos($embedPageUrl,'?') !== false ? '&' : '?';
            $payUrl = $embedPageUrl . $cat . 'order='.$order->get_id().'&pay_service='.@$payParams['pay_service'];
        }
        else {
            $payUrl = $debt->getPayUrl();
            $userLink = $payConfig->getUserLink();
            if( $userLink && isset($opts['may_identify_user']) ){
                $wpUser = \wp_get_current_user();
                if( $wpUser && !empty($wpUser->ID) && \intval($wpUser->ID) === $debt->getUserId() ){
                    $claims = [];

                    if( $userLink === APPayConfig::userLinkFull ){
                        $claims['sub'] = $wpUser->ID;
                    }
                    $claims['email'] = $wpUser->user_email;
                    $claims['name'] = $wpUser->display_name;
                    $payParams['idtoken'] = $payClient->buildIdToken($claims);
                }
            }
            if( $debt->isPaid() || isset($opts['mute_adams_event']) ){
                $payParams['no_event']=\time()+1800;
            }
            if( !empty($payParams) ){
                $payUrl .= (strpos($payUrl,'?') === false ? '?' : '&') . http_build_query($payParams);            
            }
        }
        return $payUrl;
    }
  
//    public function do_ssl_check() {
//            if( $this->enabled == "yes" ) {
//                    if( get_option( 'woocommerce_force_ssl_checkout' ) == "no" ) {
//                            echo "<div class=\"error\"><p>". sprintf( __( "<strong>%s</strong> is enabled and WooCommerce is not forcing the SSL certificate on your checkout page. Please ensure that you have a valid SSL certificate and that you are <a href=\"%s\">forcing the checkout pages to be secured.</a>" ), $this->method_title, admin_url( 'admin.php?page=wc-settings&tab=checkout' ) ) ."</p></div>";	
//                    }
//            }		
//    }

    
    
    
    // --------------------------------------------------------
    // HOOKS
    //
    
    function onAdamsPayDebtStatusNotify( ?array $apiModel ) {

        if( !$apiModel || empty($apiModel['docId'])){
            http_response_code(400);    // Invalid
            exit();
        }

        $docId = $apiModel['docId'];
        $orderId = APDebt::extractOrderIdFromDocId( $docId );
        $order  = $orderId ? wc_get_order($orderId) : null;
        if( empty($order) ) {
            return; // Silently ignore
        }
        
        $debt = APDebt::readFromOrder( $order );
        if( !$debt ){
            $debt = APDebt::allocNew( $docId, $order );
        }
        
        $debt->updateFromApiModel( $apiModel );
        
        $payStatus = $debt->getPayStatus();
        $objStatus = $debt->getObjStatus();
        
        $debt->saveToOrder($order);
        $debtPaid = $payStatus === 'paid' && $objStatus === 'success';
        $orderPaid = $order->is_paid();
        
        if( $debtPaid ){
            $order->add_order_note( sprintf(__( 'AdamsPay debt %s paid', 'wc-adamspay' ), $docId) );
        
            if(!$orderPaid ){
                $order->payment_complete( $debt->getDocId() );
            }
            
        }
        else 
        if( !$orderPaid ){
            $curStatus = $order->get_status();
            $setStatus = $this->getPayConfig()->getUnpaidOrderStatus();

            if($setStatus && $curStatus !== $setStatus ){
                switch(  $setStatus )
                {
                case 'on-hold':
                case 'cancelled':
                case 'pending':
                    $note = sprintf( __( 'AdamsPay debt status is %s/%s', 'wc-adamspay' ),$payStatus,$objStatus);
                    $order->update_status( $setStatus, $note );
                    break;
                }
            }
        }
    }
    
    function onReturnFromAdamsPay(\WC_Order $order){
        
        $orderIsPaid = $order->is_paid() ? true : false;
        $debt = APDebt::readFromOrder($order);
        if( !$orderIsPaid && $debt ){
             
            $debtPayStatus = $debt->getPayStatus();
            $debtObjStatus = $debt->getObjStatus();
            $debtIsPaid = $debtPayStatus === 'paid';
            
            if( !$debtIsPaid ){
                // User is returning, but debt still not paid,
                // Refresh information from AdamsPay to ensure we did not miss a callback
                try {
                    $apiModel  = $this->getAdamsPayClient()->getDebt($debt->getDocId());
                    $debt->updateFromApiModel($apiModel);
                    $debtPayStatus = $debt->getPayStatus();
                    $debtObjStatus = $debt->getObjStatus();
                    $debtIsPaid = $debtPayStatus === 'paid';
                }
                catch( \Exception $e ){

                }
            }
            
            if( $debtIsPaid ){
               $order->payment_complete( $debt->getDocId());
               $orderIsPaid = true;
            }
            else 
            {
                $curStatus = $order->get_status();
                $setStatus = $this->getPayConfig()->getUnpaidOrderStatus();
                
                if($setStatus && $curStatus !== $setStatus ){
                    switch(  $setStatus )
                    {
                    case 'on-hold':
                    case 'cancelled':
                    case 'pending':
                        $order->update_status( $setStatus,  __( 'Unpaid AdamsPay debt', 'wc-adamspay' )  );
                        break;
                    }
                }
            }
        }
        
        if( $orderIsPaid ){
            WC()->cart->empty_cart();   // Revisar!!
            $url = $order->get_checkout_order_received_url();
        }
        else {
            switch( $this->getPayConfig()->getUnpaidReturnTo() )
            {
            case '@checkout-payment':$url = $order->get_checkout_payment_url();break;
            case '@account-view-order':$url = $order->get_view_order_url();break;
            default:$url =$order->get_checkout_order_received_url(); break;
            }
        }
        wp_redirect( apply_filters('adamspay_order_return_url', $url, $order, $debt) );
        die();
    }
    
    // --
    
    function wcAddInfo2AdminOrderDetails(\WC_Order $order){
        $debt =  APDebt::readFromOrder($order);
        if( !$debt ){
            return; 
        }
?><p class="form-field">
    <span>AdamsPay debt <a href="<?php echo $this->buildDebtPayUrl($debt,['may_embed_as_order'=>null,'mute_adams_event'=>true,'may_identify_user'=>false]); ?>"><?php echo $debt->getDocId(); ?></a></span>
</p><?php
    }
    
	
    function wcAddInfoToOrderItems($total_rows, \WC_Order $order, $tax_display ){
        if( null !== ($text=$this->getPayConfig()->getMsgFor(APPayConfig::msgOrderDetailDebt))
         && null !== ($debt = APDebt::readFromOrder($order)) )
        {
            $link_entry=['label' => __($text, 'woocommerce'),
                 'value' =>'<a href="'.$this->buildDebtPayUrl($debt,['may_embed_as_order'=>$order,'mute_adams_event'=>false,'may_identify_user'=>true]).'">'.$debt->getDocId().'</a>'
		];
            $keys = array_keys( $total_rows );
            $index = array_search( 'payment_method', $keys );
            if( $index === false )
            {
                $total_rows['adamspay_link'] = $link_entry;
            }
            else 
            {
                $pos = $index + 1;
            	$total_rows = array_merge( array_slice( $total_rows, 0, $pos ), ['adamspay_link'=>$link_entry], array_slice( $total_rows, $pos ) );
                
            }
	}
        return $total_rows;
    }
    // --
/*     function wcAddInfo2UserOrderDetails(\WC_Order $order){	    // Replaced by wcAddInfoToOrderItems
//         if( null !== ($text=$this->getPayConfig()->getMsgFor(APPayConfig::msgOrderDetailDebt))
//          && null !== ($debt = APDebt::readFromOrder($order)) )
//         {
//         }
//     }
*/
	
    // --
    
    function wcOrderReceivedText( $text, ?\WC_Order $order)  {
        if( $order ){
            if( $order->is_paid() ){
                $msgId = APPayConfig::msgOrderRecvPaid;
            } 
            else 
            if( $order->needs_payment() || $order->get_status() === 'on-hold'){
                $msgId = APPayConfig::msgOrderRecvPending;
            }
            else {
                return $text;
            }
            $configText = $this->getPayConfig()->getMsgFor($msgId);
            if( $configText && null !== ($debt = APDebt::readFromOrder($order)) ){
                return str_replace('{{payurl}}',$this->buildDebtPayUrl($debt,['may_embed_as_order'=>$order,'may_identify_user'=>true,'mute_adams_event'=>false]),$configText);
            }
        }
        return $text;
    }

    // --
    
    function wcUserOrderActions( $actions, \WC_Order $order)  {
        
        if(!$order->is_paid() 
            && array_key_exists('pay',$actions)){
            
            $debt =  APDebt::readFromOrder($order);
            if( $debt ){
                $actions['pay']['url'] = $this->buildDebtPayUrl($debt,['may_embed_as_order'=>$order,'may_identify_user'=>true,'mute_adams_event'=>false]);
            }
        }
        return $actions;
    }
    
  
    // --

    function wcOrderCancelled($orderId){

        $order  = wc_get_order( intval($orderId) );
        if( empty($order) ){
//            do_action( 'logger', __METHOD__."($orderId): ORDER NOT FOUND");
            return ;
        }
        
        $debt =  APDebt::readFromOrder($order);
        if( !$debt ){
//            do_action( 'logger', __METHOD__."($orderId): DEBT NOT FOUND");
            return; 
        }
        $docId = $debt->getDocId();
        
//        do_action( 'logger', __METHOD__."($orderId) => $docId");
        $debtValues = [
             'objStatus'=>['status'=>'canceled','text'=>'WC cancel']
        ];
        
        $client = $this->getAdamsPayClient();
        $apiModel = $client->putDebt($docId, $debtValues);
        if( !$apiModel ){
            // Could not cancel, maybe it is already canceled
            // Retrieve a copy
            $apiModel = $client->queryDebt($docId);   
        }
        
        if( $apiModel ){
            $debt->updateFromApiModel($apiModel);
            $debt->saveToOrder( $order );
        }
        if(is_admin() && $debt->getObjStatus() !== 'canceled'){
            $order->add_order_note( __( 'AdamsPay debt could not be cancelled, please verify', 'wc-adamspay' ) );
        }
    }

    // --------------------------------------------------------
    // Private
    
    private function apBeforeRenderAdminForm()
    {
        $adminUrl = APPayConfig::getAdamsPayAdminUrl();
        $plugin = $this->getPlugin();

        $payConfig = $this->getPayConfig();
        
        $returnUrl = $plugin->getRequestHandlerUrl( APDefs::actionPathReturn );
        $notifyUrl = $plugin->getRequestHandlerUrl( APDefs::actionPathNotify );

        
        $appInfo =$payConfig->getAppInfo();
//        $_________REMOVE____selfInfo =$payConfig->getAppInfo();// APHelper::arrayOrNull(@$this->settings[APPayConfig::optSelfInfo]);
        $apiKey = $payConfig->getApiKey(); // APHelper::stringOrNull(@$this->settings[APPayConfig::optApiKey]);
        
        
        
        
 
        $returnAlert = $returnTip = $notifyAlert = $notifyTip =  null;
        if( $apiKey ){
            if( !$appInfo ){
                
                $message  = sprintf( __( 'Failed to connect using the provided Api Key and Secret', 'wc-adamspay' ), $apiKey );
                printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
                $this->form_fields['ap_api_title']['description'] = '<b style="color:#800">'.$message.'</b>';
            }
            else
            {

                $returnOk = $notifyOk = false;
//                $appInfo = @$_________REMOVE____selfInfo['app']; 
//                $merchantInfo = @$_________REMOVE____selfInfo['merchant'];
                $merchantInfo=$payConfig->getMerchantInfo();
                
                if( empty($this->settings[APPayConfig::optDebtIdPrefix]) ){
                    $this->settings[APPayConfig::optDebtIdPrefix] = 'wc' . strtolower( substr($appInfo['slug'],0,3) ) . strtolower( substr($merchantInfo['slug'],0,3) );
                }
                
                if( empty($this->settings[APPayConfig::optDebtLabel]) ){
                    $this->settings[APPayConfig::optDebtLabel] = $appInfo['label'];

                }
                foreach( APHelper::forceArray(@$appInfo['returnUrls']) as $url ){
                    if( $url === $returnUrl){
                        $returnOk = true;
                        break;
                    }
                }
                foreach( APHelper::forceArray(@$appInfo['notifyUrls']) as $url ){
                    if( $url === $notifyUrl){
                        $notifyOk = true;
                        break;
                    }
                }
                if( !($returnOk && $notifyOk ) ){
                    $message  = sprintf( __( 'The URLs configured in your AdamsPay application do not match the ones required by your plugin', 'wc-adamspay' ), $apiKey );
                    printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
                    
                    $urlAlert =  __( 'URL does not match the one configured in the AdamsPay site', 'wc-adamspay' );
                    $copyTip =  __('Copy this URL to your AdamsPay application', 'wc-adamspay' );
                    if( !$returnOk ){
                        $returnAlert =$urlAlert;
                        $returnTip = $copyTip;
                    }
                    
                    if( !$notifyOk ){
                        $notifyAlert =$urlAlert;
                        $notifyTip = $copyTip;
                    }
                }                
            }
            
        }
        
        if( !$returnTip )$returnTip = __('Users will see this page after paying', 'wc-adamspay' );
        if( !$notifyTip )$notifyTip = __('AdasmPay notifies events by posting to this URL', 'wc-adamspay' );

        $this->form_fields['ap_urls']['adamspay'] = [
            'urls'=>[
              ['label'=>'Return URL','desc_tip'=>$returnTip,'description'=>null,'value'=> $returnUrl,'description'=>$returnAlert]
             ,['label'=>'Webhook URL','desc_tip'=>$notifyTip,'description'=>null,'value'=> $notifyUrl,'description'=>$notifyAlert]
             ,['label'=>'Admin URL','desc_tip'=>null,'description'=>__('Configure your application and enable payment services using this link', 'wc-adamspay' ),'asLink'=>true,'value'=> $adminUrl]
            ]
        ];
        
        
        $paymentPageId =  APHelper::stringOrNull(@$this->settings[APPayConfig::optEmbedPaymentPage]);
        
        if( $paymentPageId ){
            /** @var \WP_Post $page */
            $page = \get_page($paymentPageId);
           if($page &&  !has_shortcode($page->post_content, APDefs::shortCodePayment) ){
                $message  = sprintf( __( 'The selected payment page "%1$s" does not contain the short-code "%2$s"', 'wc-adamspay' ), esc_html($page->post_title),esc_html( APDefs::shortCodePayment));
                printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
              
           }
        }
 
    }

    private function apAfterSettingsUpdated(){
        
        APPlugin::tryCreatePaymentPage();
        
        
        $curSelfInfo = isset( $this->settings[APPayConfig::optSelfInfo]  ) ?  $this->settings[APPayConfig::optSelfInfo]  : null;
        
        $envSlug = !empty($this->settings[ APPayConfig::optActiveEnv ])  ? $this->settings[ APPayConfig::optActiveEnv ] : null;
        $apiKey = !empty($this->settings[ APPayConfig::optApiKey ])  ? $this->settings[ APPayConfig::optApiKey ] : null;
        if( $apiKey && $envSlug ){
            $now = time();
            $infoKey = $apiKey.'@'.$envSlug.'v1';
            
            $updateSelfInfo = !$curSelfInfo 
                    || empty($curSelfInfo['key']) 
                    || empty($curSelfInfo['updated']) 
                    || $curSelfInfo['key'] !== $infoKey
                    || ($now - $curSelfInfo['updated']) > 60;   // Update every 60 seconds
            
            if( $updateSelfInfo )
            {
                try {
                    $apiModel = $this->getAdamsPayClient()->getSelf( true );
                    $app = $apiModel['app'];
                    $merchant = $apiModel['merchant'];
                    $newSelfInfo = [
                         'key'=>$infoKey
                        ,'updated'=>$now
                        ,'app'=>$app
                        ,'merchant'=>$merchant
                        ];
                }
                catch( \Exception $e ){
                    // Could not refresh self
                    $newSelfInfo = null;    
                }
            }
        }
        else {
            $updateSelfInfo = !empty($curSelfInfo);
            $newSelfInfo = null;
        }
        
        if( $updateSelfInfo){
            
            $this->update_option(APPayConfig::optSelfInfo,$newSelfInfo);
            $this->invalidateConfig();   // Force config rebuild cache
        }
    }

    
  
    private function apAfterSettingsLoaded() {
        
        if( isset($this->settings[APPayConfig::deprecated_optStagingFlag]) ){
            $this->settings[APPayConfig::optActiveEnv] = $this->settings[APPayConfig::deprecated_optStagingFlag] === 'yes' ? APDefs::testEnv : APDefs::prodEnv;
            unset( $this->settings[APPayConfig::deprecated_optStagingFlag] );
        }
        
        unset( $this->settings[APPayConfig::deprecated_optAppInfo] );
        unset( $this->settings[APPayConfig::deprecated_optMerchantInfo] );
        
    }
    
    private function invalidateConfig() {
        $this->debugLog(__FUNCTION__.'.havePayConfig:'.($this->apPayConfig?'YES':'no'));
        $this->apPayConfig = null;
    }
    


    private function _settingsStr( $name ):?string{
        if(!empty($this->settings[$name])  && is_string($this->settings[$name])){
            return $this->settings[$name];
        }
        if(!empty($this->form_fields[$name]['default']) && is_string($this->form_fields[$name]['default'])){
            return $this->form_fields[$name]['default'];
        }
        return null;
    }



}
