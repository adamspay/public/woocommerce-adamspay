<?php
namespace AdamsPay;

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
    exit;
}

include_once ADAMSPAY_INCLUDE_PATH . 'plugin.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/error.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/ship-config.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/ship-option.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/ship-quote.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/ship-request.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/ship-package.php';
include_once ADAMSPAY_INCLUDE_PATH . 'util/client.php';
include_once ADAMSPAY_INCLUDE_PATH . 'util/plugin-module.php';
include_once ADAMSPAY_INCLUDE_PATH . 'util/main-instance-module.php';

use AdamsPay\APPlugin;
use AdamsPay\APPluginModule;
use AdamsPay\APClient;
use AdamsPay\APShipConfig;
use AdamsPay\APShipPackage;
use AdamsPay\APShipOption;
use AdamsPay\APShipRequest;
use AdamsPay\APMainInstanceModule;
use AdamsPay\APException;
use \WC_Order;
use \WP_Error;

// Note that AdamsPay notifications sent to payment gateway are handled
// by APOrderHandler

/**
 * @property APPayConfig $apConfig
 */
class APShippingMethod
extends \WC_Shipping_Method 
implements APPluginModule {

    use APMainInstanceModule;  // Is connected to main instance
    
    const methodId='wc_adamspay_shipping';
    private  $apConfig;
    private static $apState = [];

    function getAdamsPayClient(): ?APClient {
        return $this->getPlugin()->getAdamsPayClient();
    }
    
    // APShipConfig is the way other modules "see" the shipping configuration
    // It keeps them isolated from the ways we encode/store the WC settings
    function getShipConfig():APShipConfig {
        
        if( !$this->apConfig ){
            // Check that we've already loaded the WC settings
            if( !is_array( $this->settings ) ){     
                $this->init_settings();     
            }
            $this->apConfig = new APShipConfig( $this->settings );
        }
        return $this->apConfig;
    }
    
    
    // Called when the plugin has been initialized
    // to register our hooks, short codes, etc.
    static function onPluginInit( APPlugin $plugin ){
        // Register shipping method
        add_filter('woocommerce_shipping_methods', function ($methods) {$methods[] = self::class;return $methods;});
        
        // Add new order filter
        add_action('woocommerce_new_order',  [self::class,'wcOrderCreated'] );
        add_action('woocommerce_order_status_completed', [self::class, 'wcOrderComplete']  );
        add_action('woocommerce_order_status_processing', [self::class, 'wcOrderProcessing']  );
        //add_action('woocommerce_after_checkout_validation', [self::class, 'wcCheckoutValidate'],10,2 );
        add_action('woocommerce_after_checkout_validation', [self::class, 'wcAfterCheckoutValidation'], 20, 2);
        add_filter('woocommerce_checkout_fields',[self::class, 'wcCustomCheckoutFields']);
        

    }
      
   function __construct() {
        self::$instance = $this;
        parent::__construct();

        $this->id                 = self::methodId; // Id for your shipping method. Should be uunique.
        $this->method_title       = 'AdamsPay';  // Title shown in admin
        $this->method_description = __('AdamsPay shipping connector', 'wc-adamspay'); // Description shown in admin

        // Load the settings API
        $this->wcInitSettingsApi();
        
   }

   
    private static $instance;
   
    private static function getInstance():self {
       if( self::$instance){
           $self = self::$instance;
           $self->debugLog(__FUNCTION__.': instance exists');
       }
       else {
           $self = new APShippingMethod();
       }
       return $self;
   }
   // ------------------------------------------------------
   // SHIPPING METHOD WC-API
   // ------------------------------------------------------

   
   /** @var WP_Error $errors */
    
   
    static function wcOrderComplete($orderId){
        self::getInstance()->_wcOrderStatus($orderId,'complete');
    }
    static function wcOrderProcessing($orderId){
        self::getInstance()->_wcOrderStatus($orderId,'processing');
    }
    
    static function wcAfterCheckoutValidation($fields, $errors) {
        self::getInstance()->_wcAfterCheckoutValidation($fields,$errors);
    }
    static function wcCustomCheckoutFields($fields) {
        return self::getInstance()->_wcCustomCheckoutFields($fields);
    }
    
    private static function getWCShippingRateId():?string {
        $ids = \wc_get_chosen_shipping_method_ids();
        return count($ids) ? $ids[0] : null;
    }
    
   
    
    
    private function _wcOrderStatus( ?string $orderId, string $status ){ 
        $order  = wc_get_order( intval($orderId) );
        if( empty($order) ){
            $this->debugLog(__METHOD__."($orderId): ORDER NOT FOUND");
            return ;    
        }
        $shipOpt = APShipOption::readFromOrder($order);
        if( !$shipOpt ){
            $this->debugLog(__METHOD__."($orderId): OPTION NOT FOUND");
            return ;
        }
        if( $this->getShipConfig()->getAutoConfirmStatus() === $status ){
            $trackingNumber = $this->confirmShipOption($order,$shipOpt );
            if( $trackingNumber ){
                $shipOpt->setRemoteTrackingNumber( $trackingNumber );
                $shipOpt->updateOrder( $order );
                
            }
        }
    }

    static function wcOrderCreated($order_id)
    {
        $sidPackage = APShipPackage::readFromSession();
        if( $sidPackage  ){
            // Get selected option id
            $rateId = self::getWCShippingRateId();
            $sidQuote = $sidPackage->getQuote();
            if( $sidQuote && $rateId ){
                // Is it an id of our quote?
                $sidOption  = $sidQuote->findOptionByRateId( $rateId );
                if( $sidOption  ){
                    // Yes: Save it to the order
                    
                     self::getInstance()->createShipQuote(new WC_Order( $order_id ),$rateId, $sidPackage,$sidOption );
                }
            }
            APShipPackage::removeFromSession();
        }
    }
    private function confirmShipOption(WC_Order $order, APShipOption $shipOpt):?string {
        try {
            $trackingNumber = $this->getAdamsPayClient()->confirmShipOption($shipOpt);
            $order->add_order_note( sprintf('AdamsPay shipping %s:%s confirmed. Ref Num. %s',$shipOpt->getProvider(),$shipOpt->getServiceId(),$trackingNumber) );
            return $trackingNumber;
        }
        catch( APException $ex ){
            $order->add_order_note( sprintf('AdamsPay shipping %s:%s confirm error:: %s',$shipOpt->getProvider(),$shipOpt->getServiceId(),$ex->getMessage()) ) ;
            return null;
        }
    }
    private function createShipQuote( WC_Order $order, string $rateId, APShipPackage $sidPackage, APShipOption $sidOption ){
        $quoteOption = $note = null;
        try {
            $apRequest = $sidPackage->getRequest();
            if( $sidOption->isDropoffTitle() ){
                $dropoffAddr = $sidOption->findDropoffAddr($rateId);
                if( !$dropoffAddr ){
                     $note = sprintf(__( 'AdamsPay shipping %s:%s error: Invalid dropoff address', 'wc-adamspay' ),$quoteOption->getProvider(),$quoteOption->getServiceId()) ;
                }
            }
            else {
                $dropoffAddr = null;
            }
            if( !$note ){
                $quote = $this->getAdamsPayClient()->getShipQuote('aex', $apRequest, false);
                if( $quote ){
                    $quoteOption = $quote->findOptionById( $sidOption->getOptionId() );
                }
                if( !$quoteOption ){
                    $note = sprintf(__( 'AdamsPay shipping %s:%s error: No option selected', 'wc-adamspay' ),$quoteOption->getProvider(),$quoteOption->getServiceId()) ;
                }
                if( !$note ){
                    $quoteOption->commitToOrder($order,$apRequest,$dropoffAddr);
                    $note = sprintf(__( 'AdamsPay shipping %s:%s created', 'wc-adamspay' ),$quoteOption->getProvider(),$quoteOption->getServiceId()) ;
                }
            }
        }
        catch( \Exception $ex ) {
            $reportId = $quoteOption ? $quoteOption->getServiceId() : $sidOption->getOptionId();
            $note = sprintf(__( 'AdamsPay shipping %s:%s quote error: - %s', 'wc-adamspay' ),$sidOption->getProvider(),$reportId,$ex->getMessage()) ;
        }
        if( $note ){
            $order->add_order_note($note);
        }
    }
  
    
    // WC_Shipping_Method
    function calculate_shipping($package = array())
    {
    // [Migs]: This method keeps getting called twice during SAME REQUEST 
    // I am unsure when this happens (Maybe when it does not return something the 1st time)
    // So I am duing a quick hack of keeping the last response
        
        $errs = null;
        $selectedRateId = self::getWCShippingRateId();
        
        if( isset(self::$apState['lastCalculate'])  ){
            $lastCalculate = self::$apState['lastCalculate'];
            $requestHash = $lastCalculate['hash'];
            $apOptions =  $lastCalculate['options'];
            $selectedRateId = $lastCalculate['selected'];
            $this->debugLog( __FUNCTION__.'['.$requestHash.','.$selectedRateId.'] ALREADY CALCULATED');
        }
        else
        {
            $apRequest = APShipRequest::allocForCalculate( $this->getShipConfig(),$package );
            $requestHash = $apRequest->getHash();

            try {

                $apPack = APShipPackage::readFromCache($requestHash);
                if( $apPack && ($apQuote = $apPack->getQuote())){
                    $apPack->saveToSession(); // Ensure in session
                    $this->debugLog( __FUNCTION__.'['.$requestHash.','.$selectedRateId.'] CACHED');
                }
                else {
                    $apClient = $this->getAdamsPayClient();

                    $destAddr = $apRequest->getDestinationAddr();
                    if( empty($destAddr['cityId']) && isset($destAddr['cityName']) && isset($destAddr['countryCode']) ){
                        
                        $resolve = $this->resolveCity(  $destAddr['countryCode'] , $destAddr['cityName'], __FUNCTION__.'-dest');
                        if( $resolve ){
                            $destAddr['cityId'] = $resolve['cityId'];
                            $destAddr['cityName'] = $resolve['cityName'];
                        }
                        else {
                            $destAddr['cityId'] = null;
                            
                        }
                        $apRequest->setDestinationAddr($destAddr);
                        //$this->debugLog( __FUNCTION__.'DEST_ADDR='.print_r($destAddr,true));
                    }
                    if( empty($destAddr['cityId']) ){
                        $errs[] = 'Ciudad o país de destino inválido';
                    }

                    $origAddr = $apRequest->getOriginAddr();
                    if( empty($origAddr['cityId']) && isset($origAddr['cityName']) && isset($origAddr['countryCode']) ){
                        $resolve = $this->resolveCity(  $origAddr['countryCode'] , $origAddr['cityName'], __FUNCTION__.'-orig');
                        if( $resolve ){
                            $origAddr['cityId'] = $resolve['cityId'];
                            $origAddr['cityName'] = $resolve['cityName'];
                        }
                        else {
                            $origAddr['cityId'] = null;
                        }
                        $apRequest->setOriginAddr($origAddr);
                    }
                    if( empty($origAddr['cityId']) ){
                        $errs[] = 'Error interno: La ciudad de orígen no está configurada';
                    }
                    if( $errs ){
                        $apQuote = null;
                    }
                    else {
                        $apQuote = $apClient->getShipQuote('aex',$apRequest,true );
                        $apPack = APShipPackage::allocNew($apRequest, $apQuote);
                        $apPack->saveToCache();
                    }
                    $this->debugLog( __FUNCTION__.'['.$requestHash.','.$selectedRateId.'] REBUILT');
                }


                
                $apOptions = $apQuote && is_array($apQuote->getOptions()) ? $apQuote->getOptions() : [];

            }
            catch( APException $e ){
                $errs[] = 'Error al calcular: '.$e->getMessage();
                $this->debugLog(__METHOD__.'.error:'.$e->getMessage().' '.\print_r($e->getDebugData(),true));
                $apOptions = [];
            }
            self::$apState['lastCalculate']=['hash'=>$requestHash,'errors'=>$errs,'options'=>$apOptions,'selected'=>$selectedRateId];
        }
    
        $cc = 0;
        /** @var APShipOption $opt */
        foreach( $apOptions as $opt ){
            ++$cc;
            if( $opt->isDropoffTitle() ){
                foreach( $opt->buildDropoffWcRates() as $childRate ){
                    ++$cc;
                    $this->add_rate($childRate);
                }            
            }
            else {
                $this->add_rate($opt->getWcRate());
            }
        }
        $this->debugLog( __FUNCTION__.'['.$requestHash.','.$selectedRateId.'] RATES_ADDED='.$cc);    //OPTIONS='.\print_r($apOptions,true));
        
       
        if( $errs ){
           // wc_add_wp_error_notices(new WP_Error(1,$errs[0]));
        }
    }
    
   
       // --
    private static function normCityName( ?string $name ):?string {
        if( $name ){
            return str_replace(['á','é','í','ó','ú','ñ'],['a','e','i','o','u','n'],\mb_strtolower($name));
        }
        return null;
        
    }
    private static function mkResolveCityKey(?string $countryCode, ?string $cityName):?string {
        return $countryCode && $cityName ?  'city'.$countryCode.md5($cityName) : null;
        
    }
    private function resolveCityList(?string $countryCode, ?string $caller='?'):array {
        
        $resolveErr = $source = null;
        $transient = get_transient('adamspay_cities'.$countryCode);
        if( is_array($transient) ){
            $source ='db-cache';
            $cityList = $transient;
        }
        else {
            $this->debugLog(__FUNCTION__.":$caller:($countryCode): DOWNLOAD");
            $source = 'server';
            $cityList =[];
            try {
                    foreach( $this->getAdamsPayClient()->getCities($countryCode) as $c){
                    $cityList[] = ['id'=>strval($c['id']) ,'label'=>$c['label'],'norm'=>self::normCityName($c['label'])];
                }
                set_transient('adamspay_cities'.$countryCode,$cityList,10);
            }
            catch( \Exception $e ){
                $this->debugLog(__FUNCTION__.":$caller:($countryCode): exception ".$e->getMessage());
                $resolveErr = __('Unable to obtain city list from server', 'wc-adamspay');
            }
            
        }
        usort ($cityList,function($a,$b) { return strncasecmp($a['norm'], $b['norm'], 4);} );
        
        $asuIx = self::searchCityIx($cityList, 'Asunción');
        if( $asuIx !== false ){
            $asu = $cityList[$asuIx];
            unset( $cityList[$asuIx]);
            array_unshift( $cityList, $asu);
        }
        return ['cityList'=>$cityList,'error'=>$resolveErr, 'source'=>$source];
    }
    
    private function resolveCity(?string $countryCode, ?string $cityName, ?string $caller='?'):array {
        
        $resolveKey = self::mkResolveCityKey($countryCode, $cityName );
        if( !$resolveKey ){
            $resolveResult =  ['cityId'=>null,'cityName'=>null,'error'=>__('Need country and city to calculate shipping', 'wc-adamspay'),'source'=>'none'];
        }
        else
        if( isset(self::$apState[$resolveKey])){
            $resolveResult =  array_merge( self::$apState[$resolveKey], ['source'=>'mem'] );
        }
        else
        {
            $transient = get_transient('adamspay_ship_res'.$resolveKey) ;
            if( is_array( $transient ) ){
                $resolveResult = array_merge( $transient, ['source'=>'db-cache'] );
            }                
            else {
                $resolveResult = null;
            }
        }
        if( !$resolveResult ) {
            
            $listResolve = $this->resolveCityList( $countryCode, $caller );
            if( isset( $listResolve['error']))
            {
                $resolveErr = $listResolve['error'];
                $city =  null;
                
            }
            else
            {
                $resolveErr = null;
                $city = self::searchCity( $listResolve['cityList'], $cityName );
            }
            if( !$city && !$resolveErr ){
                $resolveErr = __('City not found', 'wc-adamspay');
            }
            $resolveResult = ['cityId'=>$city['id']??null,'cityName'=>$city['label']??null,'error'=>$resolveErr, 'source'=>'lookup-'.$listResolve['source']];
            self::$apState[$resolveKey] = $resolveResult;
            set_transient('adamspay_city'.$resolveKey,$resolveResult,30);
        }
        $this->debugLog(__FUNCTION__.":$caller:($countryCode,$cityName)=src:".$resolveResult['source'].' #'.$resolveResult['cityId'].' "'.$resolveResult['cityName'].'" err='.$resolveResult['error']);
        return $resolveResult;
    }

    private static function searchCityIx(array $cityList, ?string $nameOrOptionId) {
        
        if(strpos($nameOrOptionId, 'adams-') === 0 ){
            $id = substr($nameOrOptionId,6);
            $normCityName = self::normCityName($nameOrOptionId);
            foreach( $cityList as $ix=>$city ){
                 if( $city['id'] === $id ){
                    return $ix;
                 }
            }
        }
        else {
            $normCityName = self::normCityName($nameOrOptionId);
            foreach( $cityList as $ix=>$city ){
                 if( $city['norm'] === $normCityName ){
                    return $ix;
                 }
            }
        }
        return false;
        
    }
    private static function searchCity(array $cityList, ?string $nameOrOptId):?array {
        return ($ix= self::searchCityIx($cityList, $nameOrOptId))===false ? null : $cityList[$ix];
    }

       
   // ------------------------------------------------------
   // SETTINGS WC-API
   // ------------------------------------------------------
   // 
   // Helper
   private function wcInitSettingsApi() {
        $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
        $this->init_settings(); // This is part of the settings API. Loads settings you previously init.
        $this->apConfig = new APShipConfig( $this->settings );
        $this->title  = 'AdamsPay'; // This can be added as an setting but for this example its forced.
        $this->enabled = $this->apConfig->isEnabled() ? 'yes' : 'no';

        // Save settings in admin if you have any defined
//        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, [ $this, 'onUpdateOptions' ] );
        add_action( 'woocommerce_update_options_shipping_' . $this->id, [ $this, 'wcSaveShippingOptions' ] );
        
       
    }
    
   
    // WC_Settings_API
    function init_settings() {
        parent::init_settings();
        $this->wcAfterSettingsLoaded();
    }

    // WC_Shipping_Method
    function admin_options(){
        $this->wcBeforeRenderAdminForm();
    }

    // WC_Settings_API
    function init_form_fields() {
        //parent::init_form_fields();
        $this->form_fields = $this->wcAllocAdminFormFields();
    }

    // Hook for action:woocommerce_update_options_shipping_{id}
    function wcSaveShippingOptions(){   //onWCUpdateOptions
        $this->settings=[];
        $this->process_admin_options();
        $this->wcAfterSettingsSaved();
    }        

    // Administration form
    private function wcAllocAdminFormFields()
    {
        $countries_obj   = new \WC_Countries();
        $country_list    = $countries_obj->__get('countries');
        $default_country = $countries_obj->get_base_country();
        
  //      $default_county_states = $countries_obj->get_states($default_country);

       //$this->generate_checkbox_html($key, $data);
        return array(
                /* 'option_name' => array(
                        'title' => 'Title for your option shown on the settings page',
                        'description' => 'Description for your option shown on the settings page',
                        'type' => 'text|password|textarea|checkbox|select|multiselect',
                        'default' => 'Default value for the option',
                        'class' => 'Class for the input',
                        'css' => 'CSS rules added line to the input',
                        'label' => 'Label', // checkbox only
                        'options' => array(
                                 'key' => 'value'
                        ) // array of options for select/multiselects only
           ) */

                'enabled' => array(
                        'title' => __('Enabled', 'wc-adamspay'),
                        'description' => __('Enable this shipping gateway', 'wc-adamspay'),
                        'type' => 'checkbox',
                        'default' => 'no'
                ),
                'auto_confirm' => array(
                        'title' => __('Auto confirm', 'wc-adamspay' ),
                        'label' => __('Confirm this service request automatically', 'wc-adamspay' ),
                        'type' => 'select',
                        'label' => __('Select a status', 'wc-adamspay'),
                        'placeholder' => __('Enter something', 'wc-adamspay'),
                        'options' => [
                            ''	=> __('No', 'wc-adamspay' ),
                            'processing' => __('yes, when order is paid/marked as processing', 'wc-adamspay' ),
                            'complete' => __('yes, when order is marked as complete', 'wc-adamspay' ),
                        ],
                        'default' => ''
                ),

                '_src_entity' => array(
                        'title'	=> __('Shipping entity', 'wc-adamspay'),
                        'description' => __('Details of the sending entity', 'wc-adamspay'),
                        'type'	=> 'title'
                ),
                'src_entity_label' => array(
                        'title' => __('Name', 'wc-adamspay'),
                        'type' => 'text'
                ),
                'src_doc_number' => array(
                        'type' => 'text',
                        'title' => __('Document number', 'wc-adamspay')
                ),
                'src_email' => array(
                        'title' => __('Email', 'wc-adamspay'),
                        'type' => 'text'
                ),
                'src_phone' => array(
                        'title' => __('Phone', 'wc-adamspay'),
                        'type' => 'text'
                ),


                '_src_title' => array(
                        'title'	=> __('Origin address', 'wc-adamspay'),
                        'description' => __('Place from where the packages will be collected to be sent to the final customer', 'wc-adamspay'),
                        'type'	=> 'title'
                ),
                'src_place_label' => array(
                        'title' => __('Location name', 'wc-adamspay'),
                        'type' => 'text',
                ),
                'src_country_code' => array(
                        'title' => __('Country', 'wc-adamspay'),
                        'type' => 'select',
                        'class' => 'chzn-drop',
                        'label' => __('Select a country', 'wc-adamspay'),
                        'options' => $country_list,
                        'default' => $default_country
                ),
                'src_state_name' => array(
                        'title' => __('State', 'wc-adamspay'),
                        'type' => 'text',
                ),
                'src_city_name' => array(
                        'title' => __('City', 'wc-adamspay'),
                        'type' => 'text',
                        'default' => $countries_obj->get_base_city(),
                ),
                'src_neighbordhood_name' => array(
                        'title' => __('Neighborhood', 'wc-adamspay'),
                        'type' => 'text',
                ),
                'src_postcode' => array(
                        'type' => 'text',
                        'title' => __('Postcode', 'wc-adamspay'),
                        'default' => $countries_obj->get_base_postcode()
                ),
                'src_street_name' => array(
                        'title' => __('Address', 'wc-adamspay'),
                        'type' => 'text',
                        'placeholder' => __('Main street name (without number)', 'wc-adamspay'),
                        'default' => $countries_obj->get_base_address(),
                ),
                'src_street_number' => array(
                        'placeholder' => __('Building/Apt/House number', 'wc-adamspay'),
                        'type' => 'text',
                        'default' => $countries_obj->get_base_address(),
                ),
                'src_street_name2' => array(
                        'type' => 'text',
                        'placeholder' => __('Secondary/reference street name', 'wc-adamspay'),
                        'default' => $countries_obj->get_base_address_2()
                ),
                'src_notes' => array(
                        'title' => __('Notes', 'wc-adamspay'),
                        'type' => 'textarea',
                        'placeholder' => __('References/notes for finding the place', 'wc-adamspay'),
                        'css' => 'max-width:450px;',
                ),
                'src_coord_lat' => array(
                        'title' => __('Latitude', 'wc-adamspay'),
                        'type' => 'decimal',
                        'placeholder' => __('I.e.: -25.2931562'),
                ),
                'src_coord_lon' => array(
                        'title' => __('Longitude', 'wc-adamspay'),
                        'type' => 'decimal',
                        'placeholder' => __('I.e.: -57.6198371'),
                ),
                '_package_title' => array(
                        'title'	=> __('Default packaging', 'wc-adamspay'),
                        'description' => __('If your product doesn\'t have shipping size or shipping weight these values will be used to quote live shipping rates', 'wc-adamspay'),
                        'type'	=> 'title'
                ),
                'pck_min_weight' => array(
                        'title' => __('Min. Weight', 'wc-adamspay') . ' (kg)',
                        'type' => 'decimal',
                        'placeholder' => __('I.e. for half a kilogram put 0.500', 'wc-adamspay'),
                ),
                'pck_min_length' => array(
                        'title' => __('Min. Length') . ' (cm)',
                        'type' => 'decimal',
                        'placeholder' => __('Length', 'wc-adamspay'),
                ),
                'pck_min_width' => array(
                        'title' => __('Min. Width', 'wc-adamspay') . ' (cm)',
                        'type' => 'decimal',
                        'placeholder' => __('Width', 'wc-adamspay'),
                ),
                'pck_min_height' => array(
                        'title' => __('Min. Height', 'wc-adamspay') . ' (cm)',
                        'type' => 'decimal',
                        'placeholder' => __('Height', 'wc-adamspay'),
                ),
                'settings_title' => array(
                        'title'	=> __('Platform settings', 'wc-adamspay'),
                        'description' => __('Configure your application and enable shipping services using this link', 'wc-adamspay'),
                        'type'	=> 'title'
                ),
        );
    }

       
    // Private
    
    private function wcBeforeRenderAdminForm(){

        $originAddr = $this->getShipConfig()->getOriginAddr();
        if( empty($originAddr['cityId']) && !empty($originAddr['countryCode']) ){
            $message = sprintf(__('<b>%s</b>: Cannot resolve city ""%s" of country "%s"', 'wc-adamspay' )
                    , $this->method_title 
                    , esc_html($originAddr['cityName'])
                    , esc_html($originAddr['countryCode'])
                    );
            printf('<div class="notice notice-error is-dismissible"><p>%1$s</p></div>', $message);
        }
        
        $adminUrl = APShipConfig::getAdamsPayAdminUrl();
?>
<h2><?php echo  $this->method_title; ?></h2>
<p><?php echo  $this->method_description; ?></p>
<table class="form-table">
        <?php $this->generate_settings_html(); ?>
        <h3 class="wc-settings-sub-title "></h3>
        <tr valign="top">
                <th scope="row" class="titledesc"><label>Admin URL </label></th>
                <td class="forminp">
                        <fieldset>
                                <a target="_blank" href="<?php echo $adminUrl; ?>"><?php echo esc_html($adminUrl); ?></a>
                                <p class="description"><?php echo  __('Configure your application and enable shipping services using this link', 'wc-adamspay'); ?></p>
                        </fieldset>
                </td>
        </tr>
</table>
<?php
        // 
    }    
    
    private function wcAfterSettingsLoaded() {
        // Future settings migrated here
    }
    private function wcAfterSettingsSaved(){
//        $this->debugLog(__METHOD__.':'.\print_r($this->settings,true));
        $this->invalidateShipConfigCache();   // Force apConfig rebuild

        $apConfig = $this->getShipConfig();
        
        $originAddr = $apConfig->getOriginAddr();
        $cityResolve = $this->resolveCity($originAddr['countryCode'], $originAddr['cityName'],__FUNCTION__);
        
        if( $cityResolve['cityId'] !== @$originAddr['cityId'] ){
            $this->update_option('src_city_id',$cityResolve['cityId']);
            $this->invalidateShipConfigCache();   // Force apConfig rebuild
        }
    }
    
    private function _wcCustomCheckoutFields( $fields ) {

        $prev = isset( $fields['order'] ) && is_array($fields['order']) ? $fields['order'] : [];
        
        $fields['order'] = array_merge(['doc_number'=>[
                'label'     => __('Cédula para retirar', 'wc-adamspay'),
                'placeholder'   => _x('Cédula', 'placeholder', 'wc-adamspay'),
                'required'  => false,
                'class'     => array('form-row-wide'),
                'clear'     => true
                    ]], $prev);
        //$this->debugLog(__METHOD__.\print_r($fields,true));
        return $fields;

    }

    
    private function _wcAfterCheckoutValidation($fields, $errors) {

 // Get selected option id
        $rateId = self::getWCShippingRateId();
        if( APShipOption::isAdamsPayRateId($rateId)){
            $validateErr = null;
        //$this->debugLog(__METHOD__.':'.print_r($fields['shipping_method'],true));
        /** @var WP_Error $errors */
            if( !empty($fields['shipping_method']) ){
                $countryCode = $fields['shipping_country'] ?? null;
                $cityName = $fields['shipping_city'] ?? null;
                $cityResolve = $this->resolveCity($countryCode, $cityName,__FUNCTION__);
                if( !empty($cityResolve['error']) ){
                    $validateErr = ['code'=>'shipping','text'=>'<b>Ciudad de envío</b> es inválida','data'=>['id'=>'shipping_city']];
                }
            }        
            if( !$validateErr ){
                $phoneField  = !empty($fields['shipping_phone']) ? 'shipping_phone' :  'billing_phone';
                $phoneValue =  $fields[$phoneField] ?? '';

                $docNumberField = !empty($fields['doc_number'])  ? 'doc_number' : 'billing_doc_number';
                $docNumberValue = $fields[$docNumberField] ?? '';

                if( !$phoneField ){
                    $validateErr = ['code'=>'shipping','text'=>'<b>Teléfono del destinatario</b> es requerido','data'=>['id'=>'shipping_phone']];
                }
                else
                if( !$docNumberValue ){
                    $validateErr = ['code'=>'shipping','text'=>'<b>Documento del destinatario</b> es requerido','data'=>['id'=>'doc_number']];
                }
            }
            
            if( !$validateErr ){
                $sidOption = $optErr = null;
                
                $sidPackage = APShipPackage::readFromSession();
                if( $sidPackage  ){
                    $sidQuote = $sidPackage->getQuote();
                    if( $sidQuote ){
                        // Is it an id of our quote?
                        $sidOption  = $sidQuote->findOptionByRateId( $rateId );
                    }
                }
                if( !$sidOption ) {
                    $optErr = 'Opción de envío ('.$rateId.') inválida';
                }
                else
                if( !$sidOption->isReadyToCommit($sidPackage->getRequest())){
                    $optErr = 'Faltan datos para poder cotizar el envío';
                }
                else
                {

                    
                    $shipRequest = $sidPackage->getRequest();
                    $shipReceiver = $shipRequest->getReceiverEntity();
                    
                    $shipReceiver['phone'] = $phoneValue;
                    $shipReceiver['docNumber'] = $docNumberValue;
                        
                    $shipRequest->setReceiverEntity( $shipReceiver );
                    $sidPackage->saveToSession();
                    
                    if( $sidOption->isDropoffTitle()){
                        
                        $dropoffAddr = $sidOption->findDropoffAddr($rateId);
                        if( !$dropoffAddr ){
                            $optErr = 'Por favor elegir una de las opciones dentro de "'.$sidOption->getLabel().'"';
                        }
                        else 
                        if( !preg_match('/^\+?(595)(9[0-9]{2})([0-9\-]{6})$/', $phoneValue) ){
                            $validateErr = ['code'=>'shipping','text'=>'<b>Opción de locker</b> requiere un celular para el envío del código','data'=>['id'=>$phoneField]];
                            foreach( ['billing_phone_required','shipping_phone_required'] as $code){
                                if( $errors->get_error_message($code) ){
                                    $errors->remove($code);
                                }
                            }

                        }
                        else
                        if( strlen($docNumberValue)<4 ){
                            $validateErr = ['code'=>'shipping','text'=>'<b>Opción de locker</b> requiere la cédula para retirar','data'=>['id'=>$docNumberField]];
                        }
                    }
                }
                if( $optErr && !$validateErr){
                    $validateErr = ['code'=>'adamspay','text'=>$optErr,'data'=>null];
                }
            }
            if( $validateErr ){
                //$errors->add( 'adamspay', esc_html__($err , 'wc-adamspay' ) );
                $errors->add( $validateErr['code'], $validateErr['text'], $validateErr['data']);
    //                    $errors->add('shipping','<b>Ciudad de envío</b> es inválida',['id'=>'shipping_city']);
    //                    $done = true;
            }
        }
//        if( !$validateErr ){
//            $validateErr = ['code'=>'adamspay','text'=>'Stop now','data'=>null];
//        }
//        $this->debugLog(__METHOD__.\print_r(['errors'=>$errors,'fields'=>$fields],true));
        
        
    }
    
    private function invalidateShipConfigCache() {
        $this->apConfig = null;
    }
    
//    static function wcCustomCheckoutFields($fields) {
//        
////        $rateId = self::getWCShippingRateId();
////        if( true ||APShipOption::isAdamsPayRateId($rateId)){
////            $fields['billing']['dropoff']=[
////               'label' => __('About yourself', 'woocommerce'),
////               'placeholder' => _x('About yourself', 'placeholder', 'woocommerce'),
////               'required' => false,
////               'class' => ['form-row-wide'],
////               'clear' => true] ;
////        }
//        APDefs::debugLog(__FUNCTION__.print_r($fields,true));
//        return $fields;
//    }

}
