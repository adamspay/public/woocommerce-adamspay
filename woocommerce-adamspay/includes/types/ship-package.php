<?php
namespace AdamsPay;
/**
 * Error and Exception classes
 */

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
    exit; // Exit if accessed directly.
}

include_once ADAMSPAY_INCLUDE_PATH . 'types/property-bag.php' ;
include_once ADAMSPAY_INCLUDE_PATH . 'types/ship-request.php' ;
include_once ADAMSPAY_INCLUDE_PATH . 'util/helper.php' ;



use AdamsPay\APPropertyBag;
use AdamsPay\APShipRequest;
use AdamsPay\APShipQuote;
use AdamsPay\APHelper;

class APShipPackage
extends APPropertyBag
{
    private const sidName = 'adamspay_ship'   // wc_adamspay_shipping_quotation
            ,transientName = 'adamspay_ship';
    
    private $request,$quote;
    protected function __construct(APShipRequest $request, APShipQuote $quote) {
        $this->request = $request; 
        $this->quote = $quote;
    }

    function getRequestHash():?string {
        return $this->request->getHash();
    }
    function getRequest():APShipRequest {
        return $this->request;
    }
    function getQuote():APShipQuote {
        return $this->quote;
    }
   
    
    static function allocNew(APShipRequest $request, APShipQuote $quote):self {
       return new APShipPackage($request,$quote);
    }

    function toStorableArray():array {
        return ['props'=>$this->_getAll(),'request'=>$this->request->toStorableArray(),'quote'=>$this->quote->toStorableArray()];
    }

    static function fromStoredArray( $stored ):?self {
        if( $stored && is_array($stored) 
                && array_key_exists('props', $stored) 
                && array_key_exists('request', $stored) 
                && array_key_exists('quote', $stored) 
                && ($request = APShipRequest::fromStoredArray($stored['request']))
                && ($quote = APShipQuote::fromStoredArray($stored['quote']))
                ){
            $self = new APShipPackage($request,$quote);
            $self->_setAll( $stored['props']);
            return $self;
        }
        return null;
    }
    
    static function readFromCache(string $requestHash):?self {
        $self = self::readFromSession();
        if( $self && $self->getRequestHash() === $requestHash )
            return $self;
        $cacheKey = self::mkCacheName( $requestHash );
        if( ($self=self::fromStoredArray(get_transient($cacheKey))) 
            && $self->getRequestHash() === $requestHash ){
                return $self;
            }

        return null;
    }
    function saveToCache() {
        $this->saveToSession();
        $cacheKey = self::mkCacheName( $this->getRequestHash() );
        set_transient($cacheKey,  $this->toStorableArray(), 300);  // preserve 5 minutes
    }
    
    static function readFromSession():?self {
        return self::fromStoredArray(WC()->session->get( self::sidName ));
    }
    function saveToSession() {
        return WC()->session->set( self::sidName, $this->toStorableArray() );
    }
    static function removeFromSession() {
         WC()->session->__unset( self::sidName  );
    }
    private static function mkCacheName(string $requestHash):string {
        return self::transientName.APHelper::getWCSessionId().'.'.$requestHash;
    }
}

