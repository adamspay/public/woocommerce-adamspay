<?php
namespace AdamsPay;
/**
 * Error and Exception classes
 */

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
    exit; // Exit if accessed directly.
}

include_once ADAMSPAY_INCLUDE_PATH . 'types/property-bag.php' ;
include_once ADAMSPAY_INCLUDE_PATH . 'types/ship-request.php';

use AdamsPay\APPropertyBag;
use AdamsPay\APShipRequest;
use \WC_Order;

class APShipOption
extends APPropertyBag
{
    
    private const wcRateIdPrefix = 'adamspay-';
    private const dropoffIdSeparator = '-dropoff-';
    
    private $provider, $service;
    
    protected function __construct(string $provider, array $service) {
        $this->provider = $provider;
        $this->service = $service;
        $this->_set('isDropoffTitle',!empty($service['dropoffs']));
    }

    static function isAdamsWcRateId(?string $wcRateId):bool {
        return $wcRateId && strpos($wcRateId,self::wcRateIdPrefix) === 0;
    }
    function getProvider():?string {
        return $this->provider;
    }
    function getOptionId():string {    // Valid always
        return $this->service['id'];
    }
    function getServiceId():?string {       // Only valid if service was quoted
        return $this->service['serviceId'];
    }
    function getLabel():?string {
        return $this->service['label'];
    }
    function getService():?array{
        return $this->service;
    }
    function setService( array $service ){
        $this->service=$service;
    }
    function containsWcRateId(?string $rateId ):bool {
        return strpos($rateId,$this->_get('wcRate')['id']) === 0;
    }
    function getWcRateId():string {
        return $this->_get('wcRate')['id'];
    }
        
    function getRemoteTrackingNumber():?string {    // Valid always
        return $this->_get('rTrackingNumber');
    }
    function setRemoteTrackingNumber(?string $id):void {       // Only valid if service was quoted
        $this->_set('remoteTrackingNumber',$id);
    }
    function getWcRate():array{
        return $this->_get('wcRate');
    }
   
    
    // These are copied from request
    function getOriginAddr():?array {
        return $this->_get('origAddr');
    }
    function setOriginAddr(?array $addr ) {
        $this->_set('origAddr',$addr);
    }
    function getSenderEntity():?array {
        return $this->_get('senderEntity');
    }
    function setSenderEntity(?array $entity ) {
        $this->_set('senderEntity',$entity);
    }
    
    function getDestinationAddr():?array {
        return $this->_get('destAddr');
    }
    function setDestinationAddr(?array $addr ) {
        $this->_set('destAddr',$addr);
    }
    function getReceiverEntity():?array {
        return $this->_get('receiverEntity');
    }
    function setReceiverEntity(?array $entity ) {
        $this->_set('receiverEntity',$entity);
    }
    function getDropoffAddr():?array {
        return $this->_get('dropoffAddr');
    }
    function setDropoffAddr(?array $addr ) {
        $this->_set('dropoffAddr',$addr);
    }

    
    function isDropoffTitle(): bool {
        return $this->_get('isDropoffTitle')  === true;
    }
    
    
    function buildDropoffWcRates():array{
        $dropRates = [];
        if( ($addrList = $this->getDropoffAddrList())!== null ){
            $wcRate = $this->_get('wcRate');
            $ratePrefix = $wcRate['id'] . self::dropoffIdSeparator ;
            $label = $wcRate['label'];
            $cost = $wcRate['cost'];
            foreach( $addrList as $dropAddr){
                $dropRates[] = [
                    'id' =>$ratePrefix . $dropAddr['id'],
                    'label' => $label.' '.\strip_tags($dropAddr['label']),
                    'cost' => $cost,
                    'calc_tax' => 'per_item'
                    ];
            }
            
        }
        return $dropRates;
    }
    function findDropoffAddr(string $rateId): ?array  {
        
        // a dropoff rate id for this option should start with the 
        // option's rate id + self::dropoffIdSeparator 
        $expectedPrefix = $this->getWcRateId() . self::dropoffIdSeparator ;
        if( strpos($rateId,$expectedPrefix) === 0
            && ($addrList = $this->getDropoffAddrList())!== null ){
            
            $addrId = substr( $rateId, strlen($expectedPrefix) );
            foreach( $addrList as $dropAddr){
                if( $dropAddr['id'] === $addrId ){
                    return $dropAddr;
                }
            }
        }
        return null;

    }
    
    function isReadyToCommit( APShipRequest $request  ):bool {
        // TODO, at low PRIO because the default WC will not allow the commit: 
        // Control also entities and other information
        // We should add those controls for special modified skins/installations
        $dest = $request->getDestinationAddr();
        $orig = $request->getOriginAddr();
        return $dest && $orig && !empty($dest['cityId']) && !empty($orig['cityId']);
    }
    static function isAdamsPayRateId( $rateId):?string {
        if( is_string($rateId) ){
            return strpos($rateId,self::wcRateIdPrefix) === 0 ? $rateId : null;
        }
        if( is_array($rateId) ){
            foreach($rateId as $id){
                if( self::isAdamsPayRateId($id))return true;
            }
        }
        return false;
    }
    
    
    function toStorableArray():array {
        return ['props'=>$this->_getAll(),'provider'=>$this->provider,'service'=>$this->service];
    }
    static function fromStoredArray( $stored ):?self {
        if( $stored && is_array($stored) && !empty($stored['provider'])  && !empty($stored['service'])){
            $opt = new APShipOption($stored['provider'],$stored['service']);
            if( !empty($stored['props']) ){
                $opt->_setAll( @$stored['props']) ;
            }
            return $opt;
        }
        return null;
    }
    
    static function allocNew( string $provider, array $service ):self {
        $self = new APShipOption($provider,$service);
        $self->_set('wcRate',[
            'id' =>APShipOption::wcRateIdPrefix . $service['id'],
            'label' => \strip_tags($service['label']  ?: $service['description']) ,
            'cost' => $service['amount']['value'],
            'calc_tax' => 'per_item'
            ]);


        return $self;
    }
    
   

    
    function commitToOrder( WC_Order $order, APShipRequest $request , ?array $dropoffAddr ){
        // Preserve request information that was used to create the request
        $this->setOriginAddr($request->getOriginAddr());
        $this->setSenderEntity($request->getSenderEntity());
        $this->setReceiverEntity($request->getReceiverEntity());
        $this->setDestinationAddr( $request->getDestinationAddr() );
        
        // Save information that will be sent to server
        $this->setDropoffAddr( $dropoffAddr );
        unset( $this->service['dropoffs'] );    // Discard dropoffs to reduce size as we will no longer need them
        $id = $order->get_id();
        update_post_meta($id, 'wc_adamspay_ship_id', $this->getServiceId());
        update_post_meta($id, 'wc_adamspay_ship', $this->toStorableArray());
    }
    function updateOrder( WC_Order $order ){
        update_post_meta($order->get_id(), 'wc_adamspay_ship', $this->toStorableArray());
    }
    
    static function readFromOrder( WC_Order $order ):?APShipOption {
        $id = $order->get_id();
        
        $stored = get_post_meta($id,'wc_adamspay_ship',true);
        if( $stored ){
            if( is_array($stored)) {
                return APShipOption::fromStoredArray( $stored );
            }
        }
        return null;
    }
    
    
    private function getDropoffAddrList():?array {
        return !empty($this->service['dropoffs']) && is_array($this->service['dropoffs']) ? $this->service['dropoffs'] : null;
    }
    
}

