<?php
namespace AdamsPay;
/**
 * Class for reading and writing configuration
 */

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
	exit; // Exit if accessed directly.
}

include_once ADAMSPAY_INCLUDE_PATH. 'util/helper.php';

use AdamsPay\APDefs;
use AdamsPay\APHelper;


class APConfig {
    
 
    protected $settings
            ,$enabled;
    
    protected function __construct( array $settings){

        $this->settings = $settings;
        $this->enabled =isset( $settings['enabled'] ) && 'yes' ===  $settings['enabled'];
    }
    
    
    function isEnabled():bool {
        return $this->enabled;
    }
    static function getAdamsPayAdminUrl():string {
        return 'https://admin.adamspay.com';
    }
    
    protected function _str(string $name ):?string{
        return isset( $this->settings[$name] ) ? APHelper::stringOrNull( $this->settings[$name]) : null ;
    }
    
}