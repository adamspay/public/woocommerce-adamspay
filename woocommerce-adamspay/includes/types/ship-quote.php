<?php
namespace AdamsPay;
/**
 * Error and Exception classes
 */

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
    exit; // Exit if accessed directly.
}

include_once ADAMSPAY_INCLUDE_PATH . 'types/property-bag.php' ;
include_once ADAMSPAY_INCLUDE_PATH . 'util/helper.php' ;



use AdamsPay\APPropertyBag;
use AdamsPay\APShipOption;
use AdamsPay\APHelper;
use \WC_Order;

class APShipQuote
extends APPropertyBag
{    
    private $options;
    protected function __construct() {
    }

    function getOptions():?array {
        return $this->options;
    }
    function addOption( APShipOption $option) {
        return $this->options[$option->getOptionId()] = $option;
    }
    function findOptionByRateId(string $rateId):?APShipOption {
        if( $this->options ){
            /** @var APShipOption $option */
            foreach($this->options as $option ){
                if( $option->containsWcRateId( $rateId ) )return $option;
            }
        }
        return null;
    }
    function findOptionById(string $id):?APShipOption {
        if( $this->options && array_key_exists($id, $this->options)) {
            return $this->options[$id];
        }
        return null;
    }
    
  

    function toStorableArray():array {
        $optArray = [];
        if( $this->options ){
            /** @var APShipOption $option */
            foreach($this->options as $option ){
                $optArray[] = $option->toStorableArray();
            }
        }
        return ['props'=>$this->_getAll(),'options'=>$optArray,'@q'=>true];
    }

    static function fromStoredArray( $stored ):?self {
        if( $stored && is_array($stored) && isset($stored['@q'])){
            $self = new APShipQuote();
            $storedOptions = @$stored['options'];
            if(is_array($storedOptions)){
                foreach( $storedOptions as $storedOpt ){
                    $opt = APShipOption::fromStoredArray($storedOpt);
                    if( $opt )$self->options[$opt->getOptionId()] = $opt;
                }
            }
            $self->_setAll( $stored['props']);
            return $self;
        }
        return null;
    }
    
    static function allocNew():self {
       return new APShipQuote();
    }
}
