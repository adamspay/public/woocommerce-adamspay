<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Old debt format stored as META

class WC_Adamspay_Debt {

    protected $id;
    protected $slug;

    protected $label;

    protected $amount_currency;
    protected $amount_value;

    protected $target_type;
    protected $target_number;
    protected $target_label;

    protected $summary_description;
    protected $summary_items = array();

    protected $valid_start;
    protected $valid_end;

    protected $valid_interval = "P10D";

    protected $doc_status = "";

    protected $pay_url = "";
    protected $pay_status = "pending";

    public function __construct($id, $data = null){

        $this->id = $id;

//        if($data){
//
//            $this->setData($data);
//
//        }

    }

    public function setData($data){

//        $this->label    = ( isset($data['label']) ) ? $data['label'] : $this->label;
//
//        $this->amount_currency  = ( isset($data['currency']) ) ? $data['currency'] : $this->amount_currency;
//        $this->amount_value     = ( isset($data['amount']) ) ? $data['amount'] : $this->amount_value;
//
//        if(!empty($data['target'])){
//            $this->setTarget($data['target']);
//        }
//
//        $this->summary_description = ( isset($data['description']) ) ? $data['description'] : $this->summary_description;
//        
//        if(!empty($data['summary_items'])){
//            $this->summary_items = array();
//            foreach($data['summary_items'] as $k => $item){
//                $this->addSummaryItem(array(
//                    "label"     => $item['label'],
//                    "currency"  => $item['currency'],
//                    "amount"    => $item['amount']
//                ));
//            }
//        }
//
//        $this->valid_interval = (isset($data['valid_interval'])) ? strtoupper($data['valid_interval']) : "P10D";
//
//        $valid_start  = ( isset($data['valid_start']) ) ? ( ( $data['valid_start'] instanceof DateTime ) ? $data['valid_start'] : new DateTime($data['valid_start']) ) : new DateTime("NOW"); 
//        $valid_end    = ( isset($data['valid_end']) ) ? ( ( $data['valid_end'] instanceof DateTime ) ? $data['valid_end'] : new DateTime($data['valid_end']) ) : null; 
//
//        $this->setValidPeriod($valid_start,$valid_end);
//
//        $this->pay_url = ( isset($data['pay_url']) ) ? $data['pay_url'] : $this->pay_url;
//        $this->pay_status = ( isset($data['pay_status']) ) ? $data['pay_status'] : $this->pay_status;
//        $this->doc_status = ( isset($data['doc_status']) ) ? $data['doc_status'] : $this->doc_status;

    }

    public function setTarget($target){

//        $this->target_type      = $target['type'];
//        $this->target_number    = $target['number'];
//        $this->target_label     = $target['label'];

    }

    public function addSummaryItem($item_data){
        
//        $this->summary_items[] = array(
//            "label"     => $item_data['label'],
//            "currency"  => $item_data['currency'],
//            "amount"    => $item_data['amount']
//        );

    }

    public function setValidInterval($interval){
//        $this->valid_interval = $interval;
    }

    public function setValidPeriod(DateTime $start, DateTime $end = null){

//        $this->valid_start  = $start;
//        if($end){
//            $this->valid_end = $end;
//        }else{
//            $this->valid_end = new DateTime();
//            $this->valid_end->createFromFormat("Y-m-dTH:i:s+0000",$this->valid_start->format("Y-m-dTH:i:s+0000"));
//            $this->valid_end->add(new DateInterval($this->valid_interval));
//        }

    }

    public function getID(){
        return $this->id;
    }

    public function getPayUrl(){
        return $this->pay_url;
    }

    public function getPayStatus(){
        return $this->pay_status;
    }

    public function getDocStatus(){
        return $this->doc_status;
    }

    public function toJSON(){
//
//        $debt = array();
//
//        $debt["docId"]  = $this->id;
//        $debt["label"]  = $this->label;
//        $debt["slug"]   = $this->slug;
//        
//        $debt["amount"] = array(
//                "currency"  => $this->amount_currency,
//                "value"     => $this->amount_value
//            );
//        
//        $debt["target"]         = array(
//            "type"      => $this->target_type,
//            "number"    => $this->target_number,
//            "label"     => $this->target_label
//        );
//        
//        $items = array();
//
//        if(!empty($this->summary_items)){
//            foreach( $this->summary_items as $item ){
//                $items[] = array(
//                    "label"     => $item['label'],
//                    "amount"    => array(
//                        "currency"  => $item['currency'],
//                        "value"     => $item['amount']
//                    )
//                );
//            }
//        }
//
//        $debt["description"]    = array(
//            "summary"   => $this->summary_description,
//            "items"     => $items
//        );
//
//        $debt['validPeriod'] = array(
//            "start" => $this->valid_start->format("Y-m-d"),
//            "end"   => $this->valid_end->format("Y-m-d")
//        );
//
//        return json_encode( array( "debt" => $debt ), JSON_UNESCAPED_UNICODE );

    }

}