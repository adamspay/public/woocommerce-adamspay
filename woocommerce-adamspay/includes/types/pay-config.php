<?php
namespace AdamsPay;
/**
 * Class for reading and writing configuration
 */

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
	exit; // Exit if accessed directly.
}

include_once ADAMSPAY_INCLUDE_PATH. 'types/config.php' ;

use AdamsPay\APDefs;
use AdamsPay\APHelper;



class APPayConfig 
extends APConfig {
    
    const 
         gatewayId='wc_adamspay'
       , optActiveEnv = 'active_env'
       , optApiKey = 'api_key'
       , optApiSecret = 'api_secret'
       , optSelfInfo = 'self'
       , optApiUrl = 'api_url'
       , optDebtLabel = 'doc_label'
       , optDebtIdPrefix = 'doc_id_prefix'
       , optDebtValidity = 'doc_interval'
       , optUserLink = 'user_link'
       , optUnpaidOrderStatus= 'unpaid_order_status'  // Will set ORDER status on returning UNPAID debts
       , optUnpaidReturnTo = 'unpaid_return_to'
       , optMsgPrefix = 'msg_'
       , optPreSelectPayMethod = 'preselect_pay_method'
       , optEmbedPaymentPage ='embed_payment_page'
       , deprecated_optAppInfo = 'adamspay_app'
       , deprecated_optStagingFlag = 'staging'
       , deprecated_optMerchantInfo  ='adamspay_merchant'
       ;
    
    const msgOrderRecvPaid = 'order_recv_paid'
        , msgOrderRecvPending = 'order_recv_pending'
        , msgOrderDetailDebt = 'order_detail_debt'
        ;
    const userLinkInfo = 'info'
        , userLinkFull = 'full';
    const payMethodSelectCheckoutField = 'checkout-field';
 
    private $env
            ,$apiUrl
            ,$userLink
            ,$selfInfo
            ;
    
    function __construct( array $settings ){
        parent::__construct($settings);
        $this->env = isset( $settings[self::optActiveEnv] ) ?  APHelper::stringOrNull($settings[self::optActiveEnv]) : APDefs::defaultEnv;
        if( !APDefs::isValidEnv($this->env) ){
            $this->env = APDefs::defaultEnv;
        }
        $this->selfInfo = isset( $settings[self::optSelfInfo] ) ? APHelper::forceArray($settings[self::optSelfInfo]) : [] ;
        $this->userLink =  isset( $settings[self::optUserLink] ) ? APHelper::stringOrNull($settings[self::optUserLink]) :  null ;
        if( $this->userLink  && false == array_search($this->userLink , [self::userLinkInfo,self::userLinkFull])) {
            $this->userLink = null; // none
        }
    }
    
    
    function getActiveEnv():string{
        return $this->env;
    }
    function getApiKey():?string{
        return $this->_str(self::optApiKey);
    }
    
    function getApiSecret():?string{
        return $this->_str(self::optApiSecret);
    }
    function getApiUrl():?string{
        if( !$this->apiUrl){
            $this->apiUrl = isset( $this->settings[self::optApiUrl] ) ? APHelper::stringOrNull($this->settings[self::optApiUrl]) : null ;
            if( !$this->apiUrl){
                switch( $this->env )
                {
                case APDefs::prodEnv:
                    $host='checkout';
                    break;
                
                case 'dev':
                    $host='dev';
                    break;
                
                case 'local':
                    $this->apiUrl = 'https://adams-test.host/api/v1';
                    break;
                
                default:
                case APDefs::testEnv:
                    $host='staging';
                    break;
                }
                if( !$this->apiUrl ){
                    $this->apiUrl = "https://{$host}.adamspay.com/api/v1";
                }
            }
        }
        return $this->apiUrl;
    }
    function getDebtLabel():?string {
        return $this->_str(self::optDebtLabel);
    }
    function getDebtIdPrefix():string {
        $v = $this->_str(self::optDebtIdPrefix);
        return $v ?? 'wcweb';
    }
    function getDebtValidity():string {
        $v = $this->_str(self::optDebtValidity);
        return $v ?? 'P1D';
    }
    
    function getUnpaidOrderStatus():?string {
        return $this->_str(self::optUnpaidOrderStatus);
    }
    function getUnpaidReturnTo():?string {
        return $this->_str(self::optUnpaidReturnTo);
    }
    function getUserLink():?string {    // see self::userLink* options
        return $this->userLink;
    }
    
    function getSelfInfo():?array{
        return $this->selfInfo;
    }
    function getAppInfo():?array{
        return isset($this->selfInfo['app']) ? $this->selfInfo['app']: null;
    }
    function getAppServiceMap():?array {
        $appInfo = $this->getAppInfo();
        return !empty($appInfo['refs']['services']) ? APHelper::arrayOrNull($appInfo['refs']['services']) : null;
    }
    function getAppSlug():?string{
        return isset($this->selfInfo['app']['slug']) ? $this->selfInfo['app']['slug'] : null;
    }
    function getMerchantInfo():?array{
        return isset($this->selfInfo['merchant']) ? $this->selfInfo['merchant']: null;
    }
    function getMerchantSlug():?string{
        return isset($this->selfInfo['merchant']['slug']) ? $this->selfInfo['merchant']['slug'] : null;
    }
    function getMsgFor($msgId):?string{  
        return $this->_str( self::optMsgPrefix.$msgId );
    }
    function getUserSessionUrl():?string{
        return isset($this->selfInfo['app']['userSessionUrl']) ? $this->selfInfo['app']['userSessionUrl']: null;
    }
    
    function getPreSelectPayMethod():?string {
        return $this->_str( self::optPreSelectPayMethod );
    }
    function getEmbedPaymentPage():?string {
        return $this->_str( self::optEmbedPaymentPage );
    }
    
    
}