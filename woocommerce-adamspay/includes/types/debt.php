<?php
namespace AdamsPay;
/**
 * Error and Exception classes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

include_once __DIR__ . '/property-bag.php' ;
include_once __DIR__ . '/legacy-debt.php' ;

use AdamsPay\APPropertyBag;
use \WC_Adamspay_Debt as LegacyDebt;
use \WC_Order;

class APDebt
extends APPropertyBag
{
    private $uid;   // Not part of stored properties (copied from order)
    
    protected function __construct(?string $docId = null) {
        if( $docId )$this->setDocId($docId);
    }

    function getDocId():?string {
        return $this->_get('docId');
    }
    function setDocId( ?string $docId ) {
        return $this->_set('docId', $docId);
    }
    // --
    function getPayUrl(){
        return $this->_get('payURL');
    }
    function setPayUrl(?string $url){
        return $this->_set('payURL',$url);
    }
    // --
    function getPayStatus():?string{
        return $this->_get('payStatus');
    }
    function setPayStatus(string $status){
        return $this->_set('payStatus',$status);
    }
    // --
    function getObjStatus():?string{
        return $this->_get('objStatus');
    }
    function setObjStatus(?string $status){
        return $this->_set('objStatus',$status);
    }
    function getUserId():?int {
        return $this->uid;
    }
    function setUserId(?int $userId) {
        $this->uid = $userId;
    }
    
    function isPaid():bool {
        return $this->getPayStatus() === 'paid' && false === array_search($this->getObjStatus(),['canceled','expired','error']); 
    }
    
    function toStorableArray():array {
        return $this->_getAll();
    }
    
    static function fromStoredArray( $stored , int $userId = 0):?self {
        if( $stored && is_array($stored) && !empty($stored['docId'])){
            $debt = new APDebt();
            $debt->_setAll( $stored );
            $debt->uid = $userId;
            return $debt;
        }
        return null;
    }
    
    static function allocNew( string $docId, WC_Order $order ):self {
        $debt = new APDebt( $docId );
        $debt->uid = $order->get_user_id();
        return $debt;
    }
    function updateFromApiModel( array $apiModel ){
        if( $apiModel['docId'] !== $this->getDocId()){
            throw new \Exception('Model ID does not match debt ID');
        }
        $this->setPayUrl( $apiModel['payUrl']);
        $this->setPayStatus( $apiModel['payStatus']['status'] );
        $this->setObjStatus( $apiModel['objStatus']['status'] );
    } 

    static function fromLegacyDebt(LegacyDebt $old , int $userId = 0):self {
        $debt = new APDebt($old->getID() );
        $debt->setPayStatus($old->getPayStatus());
        $debt->setObjStatus($old->getDocStatus());
        $debt->uid = $userId;
        return $debt;
    }
    
    
    function saveToOrder( WC_Order $order  ){
        $id = $order->get_id();
        update_post_meta($id, 'wc_adamspay_debt_id', $this->getDocId());
        update_post_meta($id, 'wc_adamspay_debt', $this->toStorableArray());
        self::$cache[$id] = $this;
        if( count(self::$cache)>20 ){
            $firstKey = key( self::$cache );
            unset( self::$cache[$firstKey] );
        }
    }
    
    static function readFromOrder( WC_Order $order ):?APDebt {
        $id = $order->get_id();
        if( isset(self::$cache[$id])) {
            return self::$cache[$id];
        }
        
        $meta = get_post_meta($id,'wc_adamspay_debt',true);
        if( $meta ){
            if( is_array($meta)) {
                self::$cache[$id] = $debt = APDebt::fromStoredArray( $meta ,  $order->get_user_id());
                return $debt;
            }
            if( is_a($meta,LegacyDebt::class)){
                self::$cache[$id] = $debt = APDebt::fromLegacyDebt( $meta ,  $order->get_user_id());
                return $debt;
            }
        }
        return null;
    }
    
    
    static function extractOrderIdFromDocId( ?string $docId ):int {
        
        if( !empty($docId) ) {
            $ix = strrpos( $docId, '-' );
            if( $ix !==  false ){
                return \intval( substr( $docId, $ix + 1 ) );
            }
        }
        return 0;
    }

    static function composeDocId( string $prefix, int $orderId ):string {
        return $prefix.'-'.$orderId;
    }
    
    
    private static $cache = [];
}

