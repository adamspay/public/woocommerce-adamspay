<?php
namespace AdamsPay;
/**
 * Base class for simple property objects
 */

if ( !defined( 'ADAMSPAY_INCLUDE_PATH') ) {
	exit; // Exit if accessed directly.
}

include_once ADAMSPAY_INCLUDE_PATH. 'util/helper.php';

use AdamsPay\APHelper;

abstract class APPropertyBag  {
    
    private $_props = [];

    protected function _get( string $name ){ return @$this->_props[$name]; }
    protected function _sub( string $name , string $sub){ return @$this->_props[$name][$sub]; }
    protected function _set( string $name , $value ){ $this->_props[$name] = $value; }
    protected function _remove( string $name ){ unset($this->_props[$name]); }
    protected function _has( string $name ):bool { return array_key_exists($name, $this->_props); }
    protected function _getAll() { return $this->_props; }
    protected function _setAll(array $props) { $this->_props = $props; }

    function clear(){ 
        $this->_props = [];
    }

    function copyFrom( APPropertyBag $rhs ){
        if( !is_a($rhs, \get_class($this)) )
                throw new \Exception("Invalid RHS for loadFrom");

        $this->_props = APHelper::cloneArray( $rhs->_props );
    }
    
    
    function _getDebugInfo() {
        return ['class'=>\get_class($this),'props'=>$this->_props];
    }

	
}