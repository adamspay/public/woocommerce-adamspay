<?php
namespace AdamsPay;
/**
 * Error and Exception classes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

include_once __DIR__ . '/property-bag.php' ;

use AdamsPay\APPropertyBag;


class APError
extends APPropertyBag
{

    const realmSystem		= 1
            , realmHttp		= 10
            , realmDatabase	= 20
            ;
    const severityInfo          = 10
            , severityWarning   = 20
            , severityError     = 30
            ;
    const reasonUnspecified         = 1
            , reasonNoData          = 10
            , reasonDuplicateData   = 20
            , reasonForbidden       = 30
            , reasonBadRequest      = 40
            , reasonNotImplemented  = 50
            ;
    
    const audiencePublic = 'public'
         ,audienceDeveloper = 'developer'
            ;
    
    private $_exception = null;
    private static $lastError = null;

    function __construct( $data = null ){
        if( $data )
            $this->initFromArray( $data );
    }
    

    static function toError( $err ):self
    {
         if( is_a($err, APError::class ))
            return $err;

         if(is_a( $err , \Exception::class))
            return new APError( ['text'=>$err->getMessage()] );
         
         if( is_string( $err ))
            return new APError( ['text'=>$err] );

         if( is_array( $err ))
            return new APError($err);

         
        return new APError(['text'=>'Unexpected error format: '.print_r($err,true)]);
    }
    
    function getRealm():?string{ return $this->_get('realm'); }
    function setRealm( ?string $realm ){ $this->_set('realm',$realm); }
    function hasRealm(){ return $this->_has('realm'); }
	
    function getSeverity():?string{ return $this->_get('severity'); }
    function setSeverity( ?string $severity ){ $this->_set('severity',$severity); }
    function hasSeverity(){ return $this->_has('severity'); }

    function getReason():?string{ return $this->_get('reason'); }
    function setReason( ?string $reason ){ $this->_set('reason',$reason); }
    function hasReason(){ return $this->_has('reason'); }

    function getInfoCode():?string{ return $this->_get('infoCode'); }
    function setInfoCode( ?string $code ){ $this->_set('infoCode',$code); }
    function hasInfoCode(){ return $this->_has('infoCode'); }

    function getHttpCode():?int{ return $this->_get('httpCode'); }
    function setHttpCode( ?int $code ){ $this->_set('httpCode',$code); }
    function hasHttpCode(){ return $this->_has('httpCode'); }
	
    function getText():?string{ return $this->_get('text'); }
    function setText( ?string $text ){ $this->_set('text',$text); }
    function hasText(){ return $this->_has('text'); }
	
    function getSubError():?APError{ return $this->_get('sub_error'); }
    function setSubError( ?APError $err ){ $this->_set('sub_error',$err); }
    function hasSubError(){ return $this->_has('sub_error'); }
	
    function getException(){ return $this->_exception; }
    function setException( $exception ){ $this->_exception = is_a( $exception, \Exception::class) ? $exception: null; }
    function hasException(){ return $this->_exception ? true:false; }

    function getAudience(){ return $this->_get('audience'); }
    function setAudience($audience){ return $this->_set('audience',$audience); }
    
    function getDebugInfo(){ return $this->_get('debugInfo'); }
    function setDebugInfo($debugInfo){ return $this->_set('debugInfo',$debugInfo); }
    
    
    function clear() {
        parent::clear();
        $this->_exception = null;
    }

    private function initFromArray( $data )
    {
        if( is_string($data))
            $data = array('text'=>$data);
        
        $this->setRealm ( array_key_exists('realm', $data) ? $data['realm'] : self::realmSystem );
        $this->setSeverity ( array_key_exists('severity', $data) ? $data['severity'] : self::severityError );
        $this->setReason ( array_key_exists('reason', $data) ? $data['reason'] : self::reasonUnspecified );
        $this->setInfoCode ( array_key_exists('infoCode', $data) ? $data['infoCode'] : null );
        $this->setHttpCode ( array_key_exists('httpCode', $data) ? $data['httpCode'] : null );
        $this->setText ( array_key_exists('text', $data) ? $data['text'] : null );
        if( array_key_exists('audience', $data)  )
            $this->setAudience ( $data['audience'] );
        
        if(array_key_exists('sub_error', $data) )
            $this->setSubError(  $data['sub_error'] );
        if(array_key_exists('exception', $data) )
            $this->setException($data['exception']);
        
        if(array_key_exists('debugInfo', $data) && $this->getAudience() === self::audienceDeveloper ){
            $this->setDebugInfo( $data['debugInfo']);
        }
    }
    function saveToArray(){
        return $this->_getAll();
    }
    function copyFrom( $rhs )
    {
        parent::copyFrom( $rhs );
        if( $rhs->_exception )$this->_exception = clone $rhs->_exception;
    }
    
    /**
     * 
     * @return APError
     */
    static function getLastError() {
        return self::$lastError;
    }
    /**
     * 
     * @param string|APError $err
     * @return APError
     */
    static function setlastError($err) {
        self::$lastError = $err ? self::toError( $err ) : null;
        return self::$lastError;
    }
    
    static function clearLastError() {
        self::$lastError = null;
    }

   

}

class APException
extends \Exception
{
    private $err, $debugData;
    
    function __construct(APError $err) {
        parent::__construct($err->getText());
        $this->err = $err;
    }
    
    function getError():APError {
        return $this->err;
    }
    
    
    static function fromError( $err ):self {
        return new APException(APError::toError($err));
    }
    
    function setDebugData( ?array $data ): self {
        $this->debugData = $data; return $this;
    }
    function addDebugField( string $name, $value ):self {
        $this->debugData[$name] = $value;
        return $this;
    }
    function getDebugData():?array {
        return $this->debugData;
    }
}
