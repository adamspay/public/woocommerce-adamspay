<?php
namespace AdamsPay;
/**
 * Error and Exception classes
 */

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
    exit; // Exit if accessed directly.
}

include_once ADAMSPAY_INCLUDE_PATH . 'types/property-bag.php' ;
include_once ADAMSPAY_INCLUDE_PATH . 'types/ship-config.php' ;

use AdamsPay\APPropertyBag;
use AdamsPay\APShipConfig;

use \WC_Product;
use \WC_Customer;

class APShipRequest
extends APPropertyBag
{
    protected function __construct() {
    }
    function getHash():?string {
        return $this->_get('@hash');
    }
    function getSignature():?string {
        return $this->_get('@signature');
    }

    
    function toStorableArray():array {
        return array_merge(['@hash'=>false],$this->_getAll());
    }
    
    static function fromStoredArray( $stored ):?self {
        if( $stored && is_array($stored) && !empty($stored['@hash'])){
            $opt = new APShipRequest();
            $opt->_setAll( $stored );
            return $opt;
        }
        return null;
    }
    
    static function allocForCalculate( APShipConfig $config, ?array $wcPackage):self {
        return (new APShipRequest())->_initForCalculate($config, $wcPackage);
    }
    function getOriginAddr():?array {
        return $this->_get('origAddr');
    }
    function setOriginAddr(?array $addr ) {
        return $this->_set('origAddr',$addr);
    }
    function getSenderEntity():?array {
        return $this->_get('senderEntity');
    }
    function setSenderEntity(?array $entity ) {
        return $this->_set('senderEntity',$entity);
    }
    
    function getDestinationAddr():?array {
        return $this->_get('destAddr');
    }
    function setDestinationAddr(?array $addr ) {
        return $this->_set('destAddr',$addr);
    }
    function getReceiverEntity():?array {
        return $this->_get('receiverEntity');
    }
    function setReceiverEntity(?array $entity ) {
        return $this->_set('receiverEntity',$entity);
    }


    function getPackages():?array {
        return $this->_get('packages');
    }
    /*
     *  
     * [country] => PY
       [state] => PY-ASU
       [postcode] => 1544
       [city] => Asuncion
       [address] => Mcal. Estigarriba 1330
       [address_1] => Mcal. Estigarriba 1330
       [address_2] => A21
     */
    private function _initForCalculate(APShipConfig $config, array $wcPackage ):self
    {
        $signature= '';
        /** @var WC_Customer $wcCustomer */
        $wcCustomer = WC()->customer;
        
        $fname = $wcCustomer->get_shipping_first_name();
        if( $fname ){
            $lname = $wcCustomer->get_shipping_last_name();
        }
        else {
            $fname = $wcCustomer->get_billing_first_name();
            $lname = $wcCustomer->get_billing_last_name();
            
        }
        $receiverEntity = [
             'firstName'=>$fname
            ,'lastName'=>$lname
            ,'phone'=>$wcCustomer->get_billing_phone()
            ,'email'=>$wcCustomer->get_billing_email()
            ,'docNumber'=>''
        ];   
        $receiverEntity['label']=\trim(\implode(' ',[$receiverEntity['firstName'],$receiverEntity['lastName']]));
        $receiverEntity['customerId']=$receiverEntity['docNumber'];

        $signature .= 'RECV='.self::signature($receiverEntity);
        $this->setReceiverEntity($receiverEntity);
        
        $senderEntity = $config->getSenderEntity();
        $signature .= 'SEND='.self::signature($senderEntity);
        $this->setSenderEntity($senderEntity);

        $originAddr = $config->getOriginAddr();
        $signature .= 'ORIG='.self::signature($originAddr);
        $this->setOriginAddr($originAddr);

        
        $destinationAddr = [];
        foreach( $wcPackage['destination'] as $wcField=>$wcValue){
            
            
            switch( $wcField )
            {
            case 'country':
                $myField = 'countryCode';
                $myValue = \strtolower($wcValue);
                break;
            case 'city':
                $myField = 'cityName';
                $myValue = \trim($wcValue);
                break;
            case 'address_1':
                $myField = 'streetName';
                $myValue = \trim($wcValue);
                if( preg_match('/^(.+)\b(\d{1,10})\b$/', $myValue, $matches) ){
                    $myValue = \trim($matches[1]);
                    $destinationAddr['streetNumber'] = $matches[2];
                }
                break;
            case 'address_2':
                $myValue = \trim($wcValue);
                if( preg_match('/^[\d]*$/', $myValue) && empty($destinationAddr['streetNumber'])){
                    $myField = 'streetNumber';
                }
                else {
                    $myField = 'streetName2';
                }
                break;
            case 'postcode':
                $myField =  'postCode';
                $myValue = \trim($wcValue);
                break;
            default:
                $myField = null;
                break;
            }
            if( $myField ){
                $destinationAddr[$myField] = $myValue;
            }        
        }
//        $destinationAddr['_d'] = $wcPackage['destination'];
        $signature .= 'DEST='.self::signature($destinationAddr).'PACK=';
        $this->setDestinationAddr($destinationAddr);
        
        $defaultDim = $config->getDefaultPackageDimensions();
        $myPackageList = [];
        $ix = 0;
        foreach( $wcPackage['contents']  as $wcContent ){
            
            $product = self::isWCProduct($wcContent['data']);
            
            if( $product && $product->needs_shipping()){
                
                if( $product->get_width() && $product->get_height() && $product->get_length() && $product->get_weight() )
                {
                    $dim = ['units'=>['dim'=>'cm','mass'=>'kg']
                            ,'width'=>$product->get_width()
                            ,'height'=>$product->get_height()
                            ,'length'=>$product->get_length()
                            ,'weight'=>$product->get_weight()
                        ];
                }
                else {
                    $dim = $defaultDim;
                }
                $myPackage = [
                         'correlationId'=>$product->get_sku()
                        ,'label'=>$product->get_name()
                        ,'declaredValue'=>['currency'=>'PYG','value'=>$product->get_price()]
                        ,'dimensions'=>$dim
                        ];
                $quantity= $wcContent['quantity'];
                for($ix=0;$ix<$quantity;++$ix) {
                    $myPackageList[] = $myPackage;
                }
                ++$ix;
                $signature.= 'p'.$ix.'-'.$product->get_id().'x'.$quantity.'v'.$myPackage['declaredValue']['value']
                            .'d'.$myPackage['dimensions']['width'].'x'.$myPackage['dimensions']['height'].'x'.$myPackage['dimensions']['length'].'x'.$myPackage['dimensions']['weight']
                        ;
            }
        }
        $this->_set('packages',$myPackageList);
        $this->_set('@signature',$signature);
        $this->_set('@hash',\md5($signature));
        return $this;
    
    }

    
    private static function signature(array $object):string {
        $s = '';
        foreach( $object as $field=>$value){
            $s.=$field.':'.self::signvalue($value);
        }
        return $s;
    
    }
    private static function signvalue($value):string {
        if(is_scalar(($value)))return strval($value);
        if(is_array(($value)))return self::signature($value);
        return '';
        
    }
    private static function isWCProduct($value):?WC_Product
    {
        if( $value && is_a($value,WC_Product::class))return $value;
        return null;
    }
 
    
}