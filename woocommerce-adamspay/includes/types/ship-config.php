<?php
namespace AdamsPay;
/**
 * Class for reading and writing configuration
 */

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
	exit; // Exit if accessed directly.
}

include_once ADAMSPAY_INCLUDE_PATH. 'types/config.php' ;

use AdamsPay\APDefs;
use AdamsPay\APHelper;


// APShipConfig is the way other modules "see" the shipping configuration
// It keeps them isolated from the ways we encode/store the WC settings

class APShipConfig 
extends APConfig {
    
    
    function __construct( array $settings ){

        parent::__construct($settings);
    }
    
    
    
    function getAutoConfirmStatus():?string {
        if( isset($this->settings['auto_confirm']) ){
            
            $v = $this->settings['auto_confirm'];
            if( is_string($v)){
                return $v;
            }
            if( is_bool($v) && $v){
                return 'complete';
            }
        }
        return null;
    }
    function getOriginAddr():array {
        $addr =  [
         'label'=>@$this->settings['src_place_label']
        ,'streetName'=>@$this->settings['src_street_name']
        ,'streetNumber'=>@$this->settings['src_street_number']
        ,'streetName2'=>@$this->settings['src_street_name2']
        ,'postCode'=>@$this->settings['src_postcode']
        ,'notes'=>@$this->settings['src_notes']
        ,'cityName'=>@$this->settings['src_city_name']
        ,'cityId'=>@$this->settings['src_city_id']
        ,'countryCode'=>strtolower( @$this->settings['src_country_code'] )
        ];     
        return $addr;
    }
    function getSenderEntity():array {
        return ['label'=>@$this->settings['src_entity_label']
            ,'firstName'=>@$this->settings['src_entity_label']
            ,'lastName'=>null
            ,'phone'=>@$this->settings['src_phone']
            ,'email'=>@$this->settings['src_email']
            ,'docNumber'=>@$this->settings['src_doc_number']
            ,'customerId'=>@$this->settings['src_doc_number']
        ];        
    }
    function getDefaultPackageDimensions():array {
        return [
             'units'=>['dim'=>'cm','mass'=>'kg']
            ,'width'=>@$this->settings['pck_min_width']
            ,'height'=>@$this->settings['pck_min_width']
            ,'length'=>@$this->settings['pck_min_length']
            ,'weight'=>@$this->settings['pck_min_weight']
        ];
    }

}