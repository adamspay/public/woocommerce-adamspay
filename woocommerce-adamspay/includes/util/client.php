<?php
namespace AdamsPay;

/**
 * Class for communicating with the AdamsPay service
 */

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
	exit; // Exit if accessed directly.
}

// Defs generates include path
include_once ADAMSPAY_INCLUDE_PATH . 'types/pay-config.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/error.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/ship-option.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/ship-quote.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/ship-request.php';
include_once ADAMSPAY_INCLUDE_PATH . 'util/helper.php';

use AdamsPay\APDefs;
use AdamsPay\APPayConfig;
use AdamsPay\APError;
use AdamsPay\APException;
use AdamsPay\APHelper;
use AdamsPay\APShipOption;
use AdamsPay\APShipRequest;
use AdamsPay\APShipQuote;

/**
 * @property APPayConfig $payConfig
 * 
 */
class APClient {

    private $payConfig;
    
    function __construct( APPayConfig $payConfig){
        $this->payConfig = $payConfig;
    }
    // --
    function getPayConfig():APPayConfig {
        return $this->payConfig;
    }
    
    function debugLog(string $msg ){
        APDefs::debugLog($msg);
        
    }
    // --
    function buildIdToken(array $claims, int $ttlSecs = 3600):string {
        
        $config = $this->getPayConfig();
        $now = time();
        
        // Cabecera estándar
        $header =['alg'=>'HS256','typ'=>'JWT'];
        $payload = array_merge([
             'iss'=>'self'
            ,'aud'=>'adamspay'
            ,'iat'=>$now
            ,'exp'=>$now + $ttlSecs
            ,'sub'=>null
            ],$claims);
        
        
        $jwtData = APHelper::base64UrlEncode(json_encode($header)) . '.' .APHelper::base64UrlEncode(json_encode($payload));
        $jwtSignature = APHelper::base64UrlEncode( \hash_hmac('sha256',$jwtData, $config->getApiSecret(), true) );
        
        return $jwtData . '.'  . $jwtSignature;
    }
    // --
  
    // --
    function getSelf( bool $verifyApiSecret = true ):array {
        $path = '/self';
        if( $verifyApiSecret ){
            $config = $this->getPayConfig();
            $secretNonce = \md5( \uniqid($config->getApiKey() . $config->getDebtLabel() ) );
            $secretHash = APHelper::base64UrlEncode( \hash_hmac('sha256', $secretNonce , $config->getApiSecret(), true)  );
            $path .='?' . \http_build_query(['secret_nonce'=>$secretNonce,'secret_hash'=>$secretHash]);
        }
        return $this->response2Json($this->execRequest( 'GET' , '/self'));
    }
 
    function getCities( string $countryCode ):array {
        return $this->parseJsonAndExtractModel($this->execRequest('GET', '/sys/countries/'.$countryCode.'/cities'), 'cities', null);
    }
    
    
    function getShipQuote( string $provider, APShipRequest $apRequest, bool $isCalculate ): APShipQuote {

        
        
        $packageModelList = [];
        foreach($apRequest->getPackages() as $package ){
            $packageModelList[] = self::mkShipPackageModel($package);
        }
        
        $reqModel =['priority'=>null
            ,'sender'=>self::mkShipEntityModel($apRequest->getSenderEntity())
            ,'origin'=>self::mkShipAddressModel($apRequest->getOriginAddr(),['label'=>'Orig'])
            ,'receiver'=>self::mkShipEntityModel($apRequest->getReceiverEntity())
            ,'destination'=>self::mkShipAddressModel($apRequest->getDestinationAddr(),['label'=>'Dest'])
            ,'packages'=>$packageModelList
            ];
    
        $config = $this->getPayConfig();
        $url = $config->getApiUrl().'/ship/providers/'.$provider.'/'.($isCalculate?'calculate':'quotes');
        
        
        
        $solicitud = ['shipping'=>$reqModel];
        $args = self::httpArgs(  'POST' , $config->getApiKey() , \json_encode($solicitud));
        $this->debugLog(__FUNCTION__.": $url\n".$args['body']);
        try {
            $httpResponse = self::httpResponse(  wp_remote_request($url,$args) ); 
            $this->debugLog(__FUNCTION__.':'.$httpResponse->get_data());
            $quoteModel = $this->extractShipQuoteModel( $httpResponse );
            $apQuote = APShipQuote::allocNew();
            foreach( $quoteModel['services'] as $service ){
                $apQuote->addOption( APShipOption::allocNew( $provider, $service) );
            }
            if( !$isCalculate )$this->debugLog(__FUNCTION__.':   '.$quoteModel['serviceId']);
            return $apQuote;
        }
        catch( APException $ex ){
            throw $this->onRequestException($ex,$args,$url);
        }
            
    }
    function confirmShipOption( APShipOption $apOption ): ?string {
        
        
        $config = $this->getPayConfig();
        $url = $config->getApiUrl().'/ship/quotes/'.$apOption->getServiceId().'/confirmations';
        $confirmModel = ['options'=>[]];
        if($apOption->getDropoffAddr()){
            $confirmModel['dropoff']=self::mkShipAddressModel($apOption->getDropoffAddr(),[],true);
        }
        $solicitud = ['confirmation'=>$confirmModel];
        $args = self::httpArgs(  'POST' , $config->getApiKey() , \json_encode($solicitud));
        $this->debugLog(__FUNCTION__.": $url\n".$args['body']);
          try {
            $httpResponse = self::httpResponse(  wp_remote_request($url,$args) ); 
            $this->debugLog(__FUNCTION__.':'.$httpResponse->get_data());
            $confirmation = $this->parseJsonAndExtractModel($httpResponse, 'confirmation', 'trackingNumber');
            return $confirmation['trackingNumber'];
        }
        catch( APException $ex ){
            throw $this->onRequestException($ex,$args,$url);
        }
            
    }
    
    
    private static function mkShipEntityModel( array $entity ):array {
        
        return [
              'correlationId'=>$entity['customerId']
            , 'docNumber'=>$entity['docNumber']
            , 'firstName'=>$entity['firstName']
            , 'lastName'=>$entity['lastName']
            , 'emailList'=>[
                ['email'=>$entity['email']]
            ]
            , 'phoneList'=>[
                ['msisdn'=>$entity['phone']]
            ]
        ];
    }
    
    private static function mkShipAddressModel( ?array $addr, array $defaults, bool $isDropoff=false ):?array {
        
        if( !$addr ){
            return null;
        }
        
        if( $isDropoff )
        {
            return ['type'=>$addr['type'],'id'=>$addr['id']];
        }
        $cityId = @$addr['cityId'];
        if( !$cityId ){
            throw APException::fromError('Address is missing city id'); 
        }
        $streetName = self::valueOrDefault('streetName',$addr, $defaults);
        $streetNumber = self::valueOrDefault('streetNumber',$addr, $defaults);
        $streetName2 = self::valueOrDefault('streetName2',$addr, $defaults);

        $text = [\trim(\implode(' ',[$streetName,$streetNumber]))];
        if( $streetName2 )$text[] =$streetName2;

        return [
         'id'=>self::valueOrDefault('id',$addr, $defaults)
        ,'type'=>self::valueOrDefault('type',$addr, $defaults)
        ,'label'=>self::valueOrDefault('label',$addr, $defaults)
        ,'text'=>$text
        ,'streetName'=>$streetName
        ,'streetNumber'=>$streetNumber
        ,'otherStreetName'=>$streetName2
        ,'aptNumber'=>null
        ,'postCode'=>self::valueOrDefault('postCode',$addr, $defaults)
        ,'cityId'=>$cityId
        ,'country'=>self::valueOrDefault('countryCode',$addr)
        ,'notes'=>self::valueOrDefault('notes',$addr, $defaults)
        ,'coords'=>self::valueOrDefault('coords',$addr, $defaults)
        ];
    }
    private static function mkShipPackageModel( array $package ):array {
        return $package;
    }
    
    private static function valueOrDefault( $property, array $values, array $defaults=[], $emptyValue =null ){
        if(array_key_exists($property, $values))return $values[$property];
        if(array_key_exists($property, $defaults))return $defaults[$property];
        return $emptyValue;
    }
    // --
    private function execRequest( string $method, string $path, ?string $body=null, ?string $contentType='application/json' ): \WP_HTTP_Requests_Response {
        $config = $this->getPayConfig();
        $url = $config->getApiUrl().$path;
        try {
            $args = self::httpArgs(  $method, $config->getApiKey(), $body, $contentType );
            return self::httpResponse(  wp_remote_request($url,$args) ); 
        }
        catch( APException $ex ){
            throw $this->onRequestException($ex,$args,$url);
        }
        
    }
    
    // --
    function postDebt(array $model, bool $updateIfExists = true):array {
        $config = $this->getPayConfig();
        $url = $config->getApiUrl().'/debts';

        $solicitud = ['debt'=>$model];
        
        $args = self::httpArgs(  'POST' , $config->getApiKey() , \json_encode($solicitud));
        
        if( $updateIfExists ){
            $args['headers']['x-if-exists'] = 'update'; // New Method (May, 2021)
            $url.='?update_if_exists=1';    // Legacy method, must remove in June, 2021
        }
        try {
            $httpResponse = self::httpResponse(  wp_remote_request($url,$args) ); 
            return $this->extractDebtModel( $httpResponse );
        }
        catch( APException $ex ){
            throw $this->onRequestException($ex,$args,$url);
        }
        
    }
    // --
    function putDebt( string $docId, array $model):array {
        $config = $this->getPayConfig();
        $url = $config->getApiUrl().'/debts/'.urlencode($docId);
        $solicitud = ['debt'=>$model];
        $args = self::httpArgs(  'PUT', $config->getApiKey() , \json_encode($solicitud));
        try {
            $httpResponse = self::httpResponse(wp_remote_request($url ,$args) ); 
            return $this->extractDebtModel( $httpResponse );
        }
        catch( APException $ex ){
            throw $this->onRequestException($ex,$args,$url);
        }
        
    }
    // --
    function getDebt( string $docId ):array {
        $config = $this->getPayConfig();
        $url = $config->getApiUrl().'/debts/'.urlencode($docId);
        $args = self::httpArgs(  'GET',$config->getApiKey() );
        
        try {
            $httpResponse = self::httpResponse(wp_remote_request($url ,$args) ); 
            return $this->extractDebtModel( $httpResponse );
        }
        catch( APException $ex ){
            throw $this->onRequestException($ex,$args,$url);
        }
    }
    // --
    function deleteDebt( string $docId ):array {
        $config = $this->getPayConfig();
        $endpoint = $config->getApiUrl().'/debts/'.urlencode($docId);
        
        $args = self::httpArgs(  'DELETE',$config->getApiKey() );
        try {
            $httpResponse = self::httpResponse(wp_remote_request($endpoint ,$args) ); 
            return $this->extractDebtModel( $httpResponse );
        }
        catch( APException $ex ){
            throw $this->onRequestException($ex,$args,$url);
        }
    }
    // --
    function requestDebtPayment( string $docId,  array $model ):array {
        $config = $this->getPayConfig();
        $url = $config->getApiUrl().'/debts/'.urlencode($docId).'/payments';
        
        $payment = ['payment'=>$model];
        $args = self::httpArgs(  'POST',$config->getApiKey() , \json_encode($payment));
        try {
            $httpResponse = self::httpResponse( wp_remote_request($url ,$args) ); 
            return $this->extractDebtModel( $httpResponse );
        }
        catch( APException $ex ){
            throw $this->onRequestException($ex,$args,$url);
        }
    }
    
    function queryUserSessionUrl():?string {
        $config = $this->getPayConfig();
        return $config->getUserSessionUrl();
    }
    // --
    function listCardsForUser( string $userId ):array {
        $config = $this->getPayConfig();
        $endpoint = $config->getApiUrl().'/users/'.urlencode($userId).'/cards';
        $args = self::httpArgs(  'GET',$config->getApiKey() );
        
        try {
            $httpResponse = self::httpResponse(wp_remote_request($endpoint ,$args) ); 
            return $this->extractCardsModel( $httpResponse );
        }
        catch( APException $ex ){
            throw $this->onRequestException($ex,$args,$url);
        }
    }
    // --
    
    private function extractShipQuoteModel( \WP_HTTP_Requests_Response $response ):array {
        return $this->parseJsonAndExtractModel($response, 'quote', 'services');
    }
    private function extractDebtModel( \WP_HTTP_Requests_Response $response ):array {
        return $this->parseJsonAndExtractModel($response, 'debt', 'docId');
    }
    private function extractCardsModel( \WP_HTTP_Requests_Response $response ):array {
        return $this->parseJsonAndExtractModel($response, 'cards', null);
    }
    
    private function parseJsonAndExtractModel(  \WP_HTTP_Requests_Response $response, string $objName, ?string $testProperty):array {
        $model = $this->response2Json($response);
        $obj = APHelper::arrayOrNull( @$model[$objName]);
        if( !$obj || ($testProperty && !array_key_exists($testProperty,$obj)) ){
            throw APException::fromError(['text'=>'Invalid response model'])->setDebugData(['model'=>$model]);
        }
        return $obj;
    }
    
    // --
    private function response2Json( \WP_HTTP_Requests_Response $response ):array {
        $json = \substr($response->get_headers()->offsetGet('content-type'),0,16) === 'application/json' ? \json_decode( $response->get_data(),true ) : null;
        if( !is_array($json) ){
            throw APException::fromError(['text'=>'Invalid response content'])->setDebugData(['response'=>$response->get_data()]);
        }
        $this->testError($response, $json);
        return $json;
        
    }
    
     /**
     * 
     * @param \WP_HTTP_Requests_Response $response
     * @param array $json
     * @throws APException
     */
    
    private function testError(\WP_HTTP_Requests_Response $response, array $json) {
        $code = \intval( $response->get_status() );
        
        if( $code < 200 || $code >= 300 ){
            if( isset($json['meta']['status']) && $json['meta']['status'] === 'error')
            {
                $meta = $json['meta'];
                $err = APError::toError([
                    'text'=>@$meta['description']
                    ,'infoCode'=>@$meta['code']
                    ,'severity'=>@$meta['severity']
                    ,'reason'=>@$meta['reason']
                ]);
            }
            else {
                $err = APError::toError('Parse error');
            }
            throw APException::fromError($err,$json);
        }
    }
    
    /**
     * 
     * @param APException $ex
     * @param array $args
     * @param string $url
     */
    private function onRequestException(APException $ex ,array $args, string $url ):APException {
        $this->debugLog(__FUNCTION__.': '.$ex->getMessage());
        $queryIx = strpos($url,'?');
        if( $queryIx !== false )$url = substr(0,$queryIx);   // Remove (potentially sensiitve) query params
        return $ex->addDebugField('request',['method'=>@args['method'],'url'=>$url]);
    }

   
    
    
    private static function httpArgs( string $method, ?string $apiKey , ?string $body = null, ?string $contentType = 'application/json'){
        $args =  [
             'method'=>$method
            ,'headers'=>[]
            ,'sslverify'=>false
            ,'timeout'=>30
        ];
        if( $apiKey )$args['headers']['apikey'] = $apiKey;
        if( $body ){
            if( $contentType )$args['headers']['content-type'] = $contentType;
            $args['body'] = $body;
            
        }
        return $args;
        
    }
     private static function httpResponse( $response ): \WP_HTTP_Requests_Response {
        if( !is_array($response)){
            /** @var \WP_Error $wpErr */
            $wpErr = $response;
            do_action('logger',__FUNCTION__.'.error',$response);
            throw APException::fromError(implode(', ',$wpErr->get_error_messages()));
        }
        return $response['http_response'];
    }
    
 
    
  
    
 }

