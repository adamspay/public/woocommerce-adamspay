<?php
namespace AdamsPay;

/**
 * Base class modules that extend the plugin
 */

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
	exit; // Exit if accessed directly.
}

use AdamsPay\APPlugin;


interface APPluginModule {
    
    // Instance of the plugin
    function getPlugin():APPlugin;

 
}

