<?php
namespace AdamsPay;
/*
 Helper class for utility functions
 */

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
	exit; // Exit if accessed directly.
}


abstract class APHelper
{
    
    private static $wc = null;
    
    static function isWooCommerceActive(): bool {
        if( self::$wc === null ){
            self::$wc = \in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) );
        }
        return self::$wc;
    }
    static function getWCSessionId(): string {
        /** @var \WC_Session $session */
        $session = WC()->session;
        $sid = $session->get('adamspay_sid');
        if( !$sid ){
            $sid = \uniqid();
            $session->set('adamspay_sid',$sid);
        }
        return $sid;
    }
    
    static function forceArray($a):array {
        return is_array($a) ? $a : [];
    }
    
    static function arrayOrNull($a):?array {
        return is_array($a) ? $a : null;
    }
    
    
    static function stringOrNull($a,bool $emptyIsNull=true):?string {
        if( !is_string($a) ){
            if( $a!==null && is_scalar( $a )){
                return strval($a);
            }
            return null;
        }
        if( $emptyIsNull && !strlen($a))return null;
        return $a;
        
    }
    
   
    
    static function toImmutableDate( ?\DateTimeInterface $date):?\DateTimeImmutable {
        if( !$date || is_a($date, \DateTimeImmutable::class))return $date;
        return \DateTimeImmutable::createFromMutable($date);
    }
    static function cloneArray( array $array ):array
    {
        $result = [];
        foreach($array as $k=>$v ) {
            if( is_array($v) )
                $result[$k] = self::cloneArray($v);
            else
            if( is_object($v ))
                $result[$k] = clone $v;
            else
                $result[$k] = $v;
        }
        return $result;
    }
    
    static function getStorableValue( $value ) {
        if( $value === null || \is_scalar($value) || \is_array($value)) {
            return $value;
        }
        if( is_a($value, \DateTimeInterface::class)){
            return self::date2Store($value);
        }
        throw new \Exception('Unstorable value');
    }
    
    static function storeValue(&$store, string $prop, $value ){
        
        $storable = self::getStorableValue( $value );
        if( $storable !== null ){
            $store[$prop] = $storable;
        }
    }
    static function date2Store( ?\DateTimeInterface  $date ):?string {
        return $date ? $date->format(\DateTime::ATOM) : null;
    }
    static function store2Date( $value ): ?\DateTimeImmutable {
        if( $value === null || !is_string($value))return null;
        return \DateTimeImmutable::createFromFormat(\DateTime::ATOM, $value);
    }
    
     static function base64UrlEncode($data) { 
        return \rtrim(\strtr(\base64_encode($data), '+/', '-_'), '='); 
    } 
    static function base64UrlDecode($data) { 
        return \base64_decode(\str_pad(\strtr($data, '-_', '+/'), \strlen($data) % 4, '=', STR_PAD_RIGHT),false); 
    } 
    
    
    private static function unquoteAndSanitizeKey(array $array, string $name):?string {
        return isset( $array[$name]) && \is_string($array[$name]) ? sanitize_key( stripslashes($array[$name]) ): null;
    }
    
    static function sanitizedQueryKey(string $name):?string {
        return self::unquoteAndSanitizeKey( $_GET, $name);
    }
    static function sanitizedRequestKey(string $name):?string {
        return self::unquoteAndSanitizeKey( $_REQUEST, $name);
    }
    static function sanitizedPostKey(string $name):?string {
        return self::unquoteAndSanitizeKey( $_POST, $name);
    }

}
