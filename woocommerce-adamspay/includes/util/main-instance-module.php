<?php
namespace AdamsPay;

/**
 * Base class modules that extend the plugin
 */

if ( ! defined( 'ADAMSPAY_INCLUDE_PATH' ) ) {
	exit; // Exit if accessed directly.
}

use AdamsPay\APDefs;
use AdamsPay\APPlugin;


trait APMainInstanceModule {
    
    
    // Instance of the plugin
    function getPlugin():APPlugin {
        return APPlugin::getMainInstance();
    }

 
    function debugLog(?string $msg){
        APDefs::debugLog($msg);
    }
}

