<?php
namespace AdamsPay;

/**
 * Global definitions/constants go here
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

define('ADAMSPAY_PLUGIN_PATH',dirname( __DIR__ ).'/' );
define('ADAMSPAY_TEMPLATE_PATH',ADAMSPAY_PLUGIN_PATH  . 'templates/');
define('ADAMSPAY_INCLUDE_PATH' ,ADAMSPAY_PLUGIN_PATH  . 'includes/');

abstract class APDefs
{
    
    const pluginName = 'wc-adamspay';    // woocommerce-adamspay'
        //, langDomain = 'wc-adamspay'
    
    const testEnv = 'test'
        , prodEnv = 'prod'
        , defaultEnv = self::testEnv
        , defaultPaymentPage = 'adams_payment';
    
    const handlerOrders = 'orders'
        , handlerPayment = 'payment'
        , handlerDebug = 'debug'
        ;
    
    const shortCodePayment = 'adamspay_payment';
    
    const actionPathNotify = 'notify'
        , actionPathReturn = 'return'
        ;
    
    static function isValidEnv(?string $envSlug):bool {
        return $envSlug === self::testEnv || $envSlug === self::prodEnv || $envSlug === 'dev' || $envSlug === 'local' ;
    }
    
    static function getEnvList():array {
        return [self::testEnv=>['label'=>'Test']
                ,self::prodEnv=>['label'=>'Producción']]
                ;
        
    }
    
    private static $_status,$_log=[];
    
    private static function getStatus():array{
        if( !self::$_status ){
            // Directorio /tmp o C:\tmp
            // Si contiene un archivo adams-dev activa el modo desarrollador
            // y crea un log
            $tmpDir = (DIRECTORY_SEPARATOR === '\\' ? 'C:' : null).DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            self::$_status = ['tmpDir'=>$tmpDir,'isDev'=>@file_exists($tmpDir.'adams-dev')];
        }
        return self::$_status;
    }
    
    static function isDevMachine(){
        return self::getStatus()['isDev'];
    }
    static function debugLog($msg,$logId='wc-plugin'){
        if( isset(self::$_log[$logId]) ){
            $log =self::$_log[$logId];
        }
        else
        {
            $status = self::getStatus();
            if( $status['isDev'] ) {
                // log to tmp dir
                self::$_log[$logId] = $log = [
                     'loopId'=>'#'.(new \DateTime())->format('Ymd H:i:s').dechex(rand(0x10,0x99))
                    ,'path'=>$status['tmpDir'].$logId.'.log'
                ];
                \error_log("\n#". $log['loopId'] . " -----\n" ,3,$log['path']);
            }
            else {
                self::$_log[$logId] = ['path'=>null];   // Logging disabled
            }
            
        }
        if( !empty($log['path'])){
            \error_log($msg."\n",3,$log['path']);
        }
    }
    
}

