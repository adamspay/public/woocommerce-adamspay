<?php

namespace AdamsPay;


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
// Defs generates include path
include_once __DIR__ . '/defs.php';
include_once ADAMSPAY_INCLUDE_PATH . 'types/pay-config.php';
include_once ADAMSPAY_INCLUDE_PATH . 'util/helper.php';
include_once ADAMSPAY_INCLUDE_PATH . 'util/plugin-module.php';
include_once ADAMSPAY_INCLUDE_PATH . 'handlers/handler.php';

use AdamsPay\APHandler;
use AdamsPay\APPayConfig;
use AdamsPay\APPluginModule;
use AdamsPay\APException;
use AdamsPay\APPaymentGateway;
use AdamsPay\APShippingMethod;
use AdamsPay\APHelper;
use WP;

/**
 * @property string $iid
 * @property string envSlug
 */
class APPlugin
implements APPluginModule
{

    const
        adminDisplayName = 'AdamsPay Gateway', pluginActivatePath = 'adamspay', routeQueryParam = 'wc_adamspay_action', formPostParam = 'adampayform';

    private $iid,$logPrefix, $apClient;

    private static $instances = [];


    function getPlugin(): APPlugin {
        return $this;
    }

    static function getMainInstance(): ?self {
        return self::allocMainInstance();
    }

    function getInstanceId(): string {
        return $this->iid;
    }

    function getPaymentGateway(): ?APPaymentGateway {
        return APPaymentGateway::getMainInstance();
    }

    function getAdamsPayClient(APPayConfig $payConfig = null): ?APClient {
        
        if( !$this->apClient ){
            if( !$payConfig ){
                if( ($gw = APPaymentGateway::getMainInstance())===null ){
                    return null;
                }
                $payConfig  = $gw->getPayConfig();
            }
            $this->apClient = new ApClient( $payConfig );
        }
        return $this->apClient;
    }

    
    function debugLog($msg,$logId='wc-plugin'){
        APDefs::debugLog($msg,$logId);
    }

    // For now, our plugin can only have one main instance
    static function allocMainInstance(): self
    {
        if (!self::$instances) {

            self::$instances['main'] = $main = new APPlugin('main');
            register_activation_hook(__FILE__, [$main, 'onPluginActivated']);
            register_deactivation_hook(__FILE__, [$main, 'onPluginDeactivated']);
            add_action('plugins_loaded', [$main, 'onPluginLoaded']);
        }
        return self::$instances['main'];
    }


    function onPluginActivated() {
        $this->debugLog(__FUNCTION__);
        flush_rewrite_rules();
        self::tryCreatePaymentPage();
    }

    function onPluginDeactivated() {
        $this->debugLog(__FUNCTION__);
        flush_rewrite_rules();
    }

    function onPluginLoaded()
    {
//JR
        load_plugin_textdomain('wc-adamspay', FALSE, 'woocommerce-adamspay/languages');
        add_action('init', [$this, 'onInitPlugin']);
    }


    function onInitPlugin()
    {

        add_action('parse_request', [$this, 'onParseRequest']);

        // Return our instance when a filter with the plugin name is applied
        add_filter(APDefs::pluginName, [$this, 'onPluginFilter']);


        // Hook admin menu actions
        add_action('admin_menu', [$this, 'onAdminMenu']);

        // Respond to handler short codes (see APHandler::getShortCodeMap for list)
        $onShortCode = [$this, 'onShortCode'];
        foreach (APHandler::getShortCodeMap() as $shortCode => $handler) {
            add_shortcode($shortCode, $onShortCode);
        }

        // Connect woocommerce
        if (APHelper::isWooCommerceActive()) {
            include_once ADAMSPAY_INCLUDE_PATH . 'payment-gateway.php';
            include_once ADAMSPAY_INCLUDE_PATH . 'shipping-method.php';
            
            
            // Register as a payment gateway
            APPaymentGateway::onPluginInit( $this );

            // Register as a shipping method
            APShippingMethod::onPluginInit( $this );
            
            
            // Create the woocommerce orders handler
            APHandler::allocHandlerWithSlug($this, APDefs::handlerOrders);
            
            
        }
        
        
        
        
        
        
    }


    // --

    function onPluginFilter(?string $request)
    {

        if ($request === null) {
            return $this;
        }

        switch ($request) {
            case 'plugin':
            case APDefs::pluginName:
                return $this;

            default:
                return $this->getHandlerWithSlug($request);
        }

        return null;
    }

    // --
    function getRequestHandlerUrl(string $actionSlug): string{
        return \get_site_url(null, '/' . self::pluginActivatePath . '/' . $actionSlug);
    }

    
    static function wpActionsLink ( $actions ) {
        $mylinks = array(
           '<a href="' . admin_url( 'admin.php?page=adamspay' ) . '">Settings</a>',
        );
        return array_merge( $actions, $mylinks );
     }

    function onParseRequest(WP $wp){

        if (preg_match('/^' . self::pluginActivatePath . '\/([^\/]{1,23})\/?([^?]{0,512})?$/', $wp->request, $matches)) {
            $actionSlug = $matches[1];
            $actionPath = @$matches[2];
            $actionMap = APHandler::getRequestActionMap();
            if (isset($actionMap[$actionSlug])) {
                $handler =  APHandler::allocHandlerWithSlug($this, $actionMap[$actionSlug]);
                if ($handler) {
                    $handler->onRequest($wp, $actionSlug, $actionPath);
                }
            }
        } else {
            $handlerSlug = APHelper::sanitizedPostKey( APHandler::fieldFormHandlerSlug );
            if ($handlerSlug) {
                $handler = APHandler::allocHandlerWithSlug($this, $handlerSlug);
                if ($handler) {
                    $handler->onShortCodePost($wp);
                }
            }
        }
        // Init Session *AFTER* handler executes
        // becaus some handlers might end execution without returning
        // Handler may decide to initialize session before it it needs it
        $this->initSession();
    }



    function onShortCode($attr, ?string $content, string $shortCode): string
    {
        $shortCodeMap = APHandler::getShortCodeMap();
        $handler =  isset($shortCodeMap[$shortCode])  ? $this->getHandlerWithSlug($shortCodeMap[$shortCode]) : null;

        if ($handler) {
            return $handler->onShortCodeRender(is_array($attr) ? $attr : [$attr], $content, $shortCode);
        }
        return 'No handler for short code ' . esc_html($shortCode);
    }


    function onAdminMenu()
    {
        //        new CCPConfigMenu( $this );    // Alloc Config Menu
        //
        $structure = get_option('permalink_structure');

        if (empty($structure)) {
            add_action('admin_notices', function () {
                $message = sprintf(
                    esc_html__('"%1$s" requires Pretty permalinks. Please check your permalink settings', 'wc-adamspay' ),'<strong>' . esc_html__(self::adminDisplayName, 'wc-adamspay') . '</strong>');
                printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);
            });
        }

        if (!APHelper::isWooCommerceActive()) {
            add_action('admin_notices', function () {
                $message = sprintf(
                    esc_html__('"%1$s" requires "%2$s" installed and activated.', 'wc-adamspay' ),
                    '<strong>' . esc_html__(self::adminDisplayName, 'wc-adamspay' ) . '</strong>',
                    '<strong>' . esc_html__('Woocommerce') . '</strong>'
                );

                printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);
            });
        }


//        add_menu_page(
//            sprintf(__('Configuración básica de %s'), 'AdamsPay'),
//            'AdamsPay',
//            'manage_options',
//            'adamspay',
//            array(&$this, 'display'),
//            plugins_url('assets/images/adamspay-icon.png', dirname(__FILE__)),
//            25
//        );
        
        add_submenu_page(
            'woocommerce',
            sprintf(__('Configuration shortcuts for %s'), 'AdamsPay'),
            'AdamsPay',
            'manage_woocommerce',
            'adamspay',
            array(&$this, 'display'),
            25
        ); 
    }

    function display()
    {
        $adminUrl = get_admin_url().'admin.php';
        //wp_enqueue_style('custom_wp_admin_css', plugins_url('assets/css/adamspay-admin-style.css', dirname(__FILE__)));
?>
        <div style="margin-top:20px;width:100%">
            <div class="card" style="width:100%">
                <img src="<?php echo plugins_url('assets/images/adams-bar-logo.png', dirname(__FILE__)); ?>" alt="" style="width: 190px; margin-top: 20px">
                <hr noshade>
                <table style="margin-top:20px">
                    <tr>
                        <td style="text-align:center; margin: 20px; width: 150px">
                            <a href="<?php echo $adminUrl; ?>?page=wc-settings&tab=checkout&section=wc_adamspay">
                                <img src="<?php echo  plugins_url('assets/images/payment-method.png', dirname(__FILE__)); ?>" alt="" style="width: 64px;"><br />
                                Cobros
                            </a>
                        </td>
                        <td style="text-align:center; margin: 20px; width: 150px">
                            <a href="<?php echo  $adminUrl; ?>?page=wc-settings&tab=shipping&section=wc_adamspay_shipping">
                                <img src="<?php echo plugins_url('assets/images/delivery.png', dirname(__FILE__)) ;?>" alt="" style="width: 64px;"><br />
                                Shipping
                            </a>
                        </td>
                        <td style="text-align:center; margin: 20px; width: 150px">
                            <a href="https://admin.adamspay.com" target="_blank">
                                <img src="<?php echo plugins_url('assets/images/settings.png', dirname(__FILE__)); ?>" alt="" style="width: 64px;"><br />
                                Servicios
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
<?php }


    /**
     * 
     * @param bool $throwExceptionOnFail
     * @return bool
     * @throws APException
     */

    function initSession(bool $throwExceptionOnFail = true): bool
    {
        if (session_status() === PHP_SESSION_ACTIVE) {
            return true;
        }

        if (session_status() === PHP_SESSION_NONE && session_start()) {
            return true;
        }

        if ($throwExceptionOnFail) {
            throw APException::fromError('Unable to start session');
        }
        return false;
    }


    function getHandlerWithSlug(string $slug): ?APHandler
    {
        return APHandler::allocHandlerWithSlug($this, $slug);
    }



    function adminNotice_NeedPermalinks()
    {

        $message = sprintf(
            esc_html__('"%1$s" requires Pretty permalinks. Please check your permalink settings', 'wc-adamspay' ),
            '<strong>' . esc_html__(self::adminDisplayName, 'wc-adamspay' ) . '</strong>'
        );

        printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);
    }
    
    static function includeTemplate(string $name):?callable{
        
        $fn = 'AdamsPay\\Template\\'.$name;
        if( function_exists($fn) )
            return $fn;

        $templatePath = get_template_directory().'/templates/adamspay/'.$name.'.php';

        if( !\file_exists($templatePath) ){
            $templatePath = ADAMSPAY_TEMPLATE_PATH.'adamspay/'.$name.'.php';   // use mine
            if( !\file_exists($templatePath) ){
                return null;
            }
        }

include_once $templatePath;

        return function_exists($fn)? $fn : null;

    }



    
    
    static function tryCreatePaymentPage()
    {
        $path = ApDefs::defaultPaymentPage;
        $page = get_page_by_path($path);
        if( $page ){
            $pageId = $page->ID;
        }
        else {
            $pageId = wp_insert_post(
                    array(
                    'comment_status' => 'close',
                    'ping_status'    => 'close',
                    'post_author'    => 1,
                    'post_title'     => __('Adams payment' , 'wc-adamspay' ),
                    'post_name'      => ApDefs::defaultPaymentPage,
                    'post_status'    => 'publish',
                    'post_content'   => '<!-- wp:paragraph -->
<p>You may modify this page to match your site if payments are done embedded or hide it otherwise.<br>
Page MUST contain the [['.APDefs::shortCodePayment.']] short code that creates the payment view!</p>
<!-- /wp:paragraph -->

<!-- wp:shortcode -->
['.APDefs::shortCodePayment.']
<!-- /wp:shortcode -->',
                    'post_type'      => 'page',
                    'post_parent'    =>  0 //'id_of_the_parent_page_if_it_available'
                    )
                );
            if( $pageId > 0){
                
                self::removePageFromMenus($pageId);
            }
        }
        return $pageId;
    }
    
    static function removePageFromMenus($pageId){

        // get menu locations
        $locations = get_nav_menu_locations();
        // cycle through (any) and get menu in slot
        if (is_array($locations)) {
            foreach ($locations as $menuID){
                // Retrieve Menu & Pages in menu
                $menu = wp_get_nav_menu_object($menuID);            
                $pagesItem = wp_get_nav_menu_items($menu, ['object'=>'page']);

                // Cycle through pages & remove target menu page link if present
                if (is_array($pagesItem)) {
                    foreach ($pagesItem as $page){
                        if ($page->object_id == $pageId) 
                            wp_delete_post($page->db_id);
                    }
                }

            }
        }

    }
 
    protected function __construct(string $instanceId)
    {
        $this->iid = $instanceId;
        $this->logPrefix = dechex(time() - 100000000);
    }
}
